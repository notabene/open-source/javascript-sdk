[**@notabene/javascript-sdk**](../README.md)

***

[@notabene/javascript-sdk](../modules.md) / types

# types

## Index

### Enumerations

- [AgentType](enumerations/AgentType.md)
- [CMType](enumerations/CMType.md)
- [ErrorIdentifierCode](enumerations/ErrorIdentifierCode.md)
- [HMType](enumerations/HMType.md)
- [PersonType](enumerations/PersonType.md)
- [ProofStatus](enumerations/ProofStatus.md)
- [ProofTypes](enumerations/ProofTypes.md)
- [Status](enumerations/Status.md)
- [ValidationSections](enumerations/ValidationSections.md)
- [VASPSearchControl](enumerations/VASPSearchControl.md)

### Interfaces

- [Agent](interfaces/Agent.md)
- [CallbackOptions](interfaces/CallbackOptions.md)
- [ComponentRequest](interfaces/ComponentRequest.md)
- [ComponentResponse](interfaces/ComponentResponse.md)
- [ConnectionRecord](interfaces/ConnectionRecord.md)
- [ConnectionRequest](interfaces/ConnectionRequest.md)
- [Counterparty](interfaces/Counterparty.md)
- [DeclarationProof](interfaces/DeclarationProof.md)
- [Deposit](interfaces/Deposit.md)
- [DepositRequest](interfaces/DepositRequest.md)
- [DepositRequestOptions](interfaces/DepositRequestOptions.md)
- [DepositTransaction](interfaces/DepositTransaction.md)
- [LegalPerson](interfaces/LegalPerson.md)
- [MicroTransferProof](interfaces/MicroTransferProof.md)
- [NaturalPerson](interfaces/NaturalPerson.md)
- [OwnershipProof](interfaces/OwnershipProof.md)
- [ScreenshotProof](interfaces/ScreenshotProof.md)
- [SignatureProof](interfaces/SignatureProof.md)
- [ThresholdOptions](interfaces/ThresholdOptions.md)
- [Transaction](interfaces/Transaction.md)
- [TransactionOptions](interfaces/TransactionOptions.md)
- [TransactionResponse](interfaces/TransactionResponse.md)
- [VASP](interfaces/VASP.md)
- [Wallet](interfaces/Wallet.md)
- [Withdrawal](interfaces/Withdrawal.md)

### Type Aliases

- [BlockchainAddress](type-aliases/BlockchainAddress.md)
- [CAIP10](type-aliases/CAIP10.md)
- [CAIP19](type-aliases/CAIP19.md)
- [CAIP2](type-aliases/CAIP2.md)
- [CAIP220](type-aliases/CAIP220.md)
- [Cancel](type-aliases/Cancel.md)
- [Completed](type-aliases/Completed.md)
- [ComponentMessage](type-aliases/ComponentMessage.md)
- [ConnectionOptions](type-aliases/ConnectionOptions.md)
- [CryptoCredential](type-aliases/CryptoCredential.md)
- [Destination](type-aliases/Destination.md)
- [DID](type-aliases/DID.md)
- [DTI](type-aliases/DTI.md)
- [Error](type-aliases/Error.md)
- [FieldOptions](type-aliases/FieldOptions.md)
- [FieldTypes](type-aliases/FieldTypes.md)
- [HostMessage](type-aliases/HostMessage.md)
- [InvalidValue](type-aliases/InvalidValue.md)
- [ISOCurrency](type-aliases/ISOCurrency.md)
- [LegalPersonFieldName](type-aliases/LegalPersonFieldName.md)
- [LegalPersonFields](type-aliases/LegalPersonFields.md)
- [LEI](type-aliases/LEI.md)
- [NaturalPersonFieldName](type-aliases/NaturalPersonFieldName.md)
- [NaturalPersonFields](type-aliases/NaturalPersonFields.md)
- [NotabeneAsset](type-aliases/NotabeneAsset.md)
- [Ready](type-aliases/Ready.md)
- [ResizeRequest](type-aliases/ResizeRequest.md)
- [Source](type-aliases/Source.md)
- [Theme](type-aliases/Theme.md)
- [TransactionAsset](type-aliases/TransactionAsset.md)
- [TravelAddress](type-aliases/TravelAddress.md)
- [UpdateValue](type-aliases/UpdateValue.md)
- [URI](type-aliases/URI.md)
- [UUID](type-aliases/UUID.md)
- [V1Asset](type-aliases/V1Asset.md)
- [V1Transaction](type-aliases/V1Transaction.md)
- [ValidationError](type-aliases/ValidationError.md)
- [VASPOptions](type-aliases/VASPOptions.md)

## References

### Address

Re-exports [Address](../ivms/types/type-aliases/Address.md)

***

### Beneficiary

Re-exports [Beneficiary](../ivms/types/type-aliases/Beneficiary.md)

***

### BeneficiaryVASP

Re-exports [BeneficiaryVASP](../ivms/types/type-aliases/BeneficiaryVASP.md)

***

### ISOCountryCode

Re-exports [ISOCountryCode](../ivms/types/type-aliases/ISOCountryCode.md)

***

### ISODate

Re-exports [ISODate](../ivms/types/type-aliases/ISODate.md)

***

### IVMS101

Re-exports [IVMS101](../ivms/types/type-aliases/IVMS101.md)

***

### NationalIdentification

Re-exports [NationalIdentification](../ivms/types/type-aliases/NationalIdentification.md)

***

### OriginatingVASP

Re-exports [OriginatingVASP](../ivms/types/type-aliases/OriginatingVASP.md)

***

### Originator

Re-exports [Originator](../ivms/types/type-aliases/Originator.md)

***

### PayloadMetadata

Re-exports [PayloadMetadata](../ivms/types/interfaces/PayloadMetadata.md)

***

### TransferPath

Re-exports [TransferPath](../ivms/types/type-aliases/TransferPath.md)
