[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / Cancel

# Type Alias: Cancel

> **Cancel**: `object`

**`Internal`**

Represents a cancel component message

## Type declaration

### type

> **type**: [`CANCEL`](../enumerations/CMType.md#cancel)

## Defined in

[types.ts:869](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L869)
