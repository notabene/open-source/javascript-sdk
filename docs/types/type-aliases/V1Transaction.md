[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / V1Transaction

# Type Alias: V1Transaction

> **V1Transaction**: `object`

Transaction payload suitable for calling Notabene v1 tx/create

## Type declaration

### beneficiary

> **beneficiary**: [`Beneficiary`](../../ivms/types/type-aliases/Beneficiary.md)

### beneficiaryProof?

> `optional` **beneficiaryProof**: [`OwnershipProof`](../interfaces/OwnershipProof.md)

### beneficiaryVASPdid

> **beneficiaryVASPdid**: [`DID`](DID.md)

### originator?

> `optional` **originator**: [`Originator`](../../ivms/types/type-aliases/Originator.md)

### originatorEqualsBeneficiary?

> `optional` **originatorEqualsBeneficiary**: `boolean`

### originatorProof?

> `optional` **originatorProof**: [`OwnershipProof`](../interfaces/OwnershipProof.md)

### originatorVASPdid

> **originatorVASPdid**: [`DID`](DID.md)

### transactionAmount

> **transactionAmount**: `string`

### transactionAsset

> **transactionAsset**: [`V1Asset`](V1Asset.md)

### transactionId?

> `optional` **transactionId**: `string`

## Defined in

[types.ts:620](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L620)
