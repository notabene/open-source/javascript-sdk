[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / DTI

# Type Alias: DTI

> **DTI**: `string`

Digital Token Identifier (DTI) following ISO 24165 standard

## Remarks

A standardized identifier for digital assets and cryptocurrencies. The DTI system
provides unique and unambiguous identification of digital tokens, supporting interoperability
and clarity in financial markets.

Format: `DTI[NNNNN]` where N is a digit

## Examples

```ts
"DTI00001" // Example DTI for Bitcoin
```

```ts
"DTI00002" // Example DTI for Ethereum
```

## See

 - [Digital Token Identifier Foundation](https://dtif.org/)
 - [ISO 24165](https://www.iso.org/standard/77895.html)

## Defined in

[types.ts:117](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L117)
