[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / InvalidValue

# Type Alias: InvalidValue\<T\>

> **InvalidValue**\<`T`\>: `object`

**`Internal`**

Represents an invalid value component message

## Type Parameters

• **T**

The overall Value type being returned

## Type declaration

### errors

> **errors**: [`ValidationError`](ValidationError.md)[]

### type

> **type**: [`INVALID`](../enumerations/CMType.md#invalid)

### value

> **value**: `Partial`\<`T`\>

## Param

The current Partial value

## Param

Array of validation errors

## Defined in

[types.ts:880](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L880)
