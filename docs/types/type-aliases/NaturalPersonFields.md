[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / NaturalPersonFields

# Type Alias: NaturalPersonFields

> **NaturalPersonFields**: `{ [name in NaturalPersonFieldName]?: FieldOptions }`

Field properties by field name for Natural persons

## Defined in

[types.ts:398](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L398)
