[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / FieldOptions

# Type Alias: FieldOptions

> **FieldOptions**: `boolean` \| \{ `optional`: `boolean`; `transmit`: `boolean`; \}

Field properties

## Defined in

[types.ts:699](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L699)
