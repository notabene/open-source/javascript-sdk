[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / ResizeRequest

# Type Alias: ResizeRequest

> **ResizeRequest**: `object`

**`Internal`**

Represents a resize request component message. This is handled by the library.

## Type declaration

### height

> **height**: `number`

### type

> **type**: [`RESIZE`](../enumerations/CMType.md#resize)

## Defined in

[types.ts:835](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L835)
