[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / NotabeneAsset

# Type Alias: NotabeneAsset

> **NotabeneAsset**: `string`

Internal identifier for assets in the Notabene system

## Remarks

A standardized string format used within Notabene to identify cryptocurrencies,
tokens, and other digital assets. This is Notabene's legacy asset identification
system that may be used alongside CAIP-19 and DTI identifiers.

## Examples

```ts
"ETH_USDT" // USDT token on Ethereum
```

```ts
"BTC" // Bitcoin
```

## See

 - [CAIP19](CAIP19.md) For chain-agnostic asset identifiers
 - [DTI](DTI.md) For ISO standardized identifiers

## Defined in

[types.ts:139](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L139)
