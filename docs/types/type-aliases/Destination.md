[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / Destination

# Type Alias: Destination

> **Destination**: [`BlockchainAddress`](BlockchainAddress.md) \| [`CAIP10`](CAIP10.md) \| [`CryptoCredential`](CryptoCredential.md) \| [`TravelAddress`](TravelAddress.md)

The destination of a transaction either a blockchain address, a CAIP-19 address, or a travel address.

## Defined in

[types.ts:197](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L197)
