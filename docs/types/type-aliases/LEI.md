[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / LEI

# Type Alias: LEI

> **LEI**: `string`

A LEI Legal Entity Identifier

## Defined in

[types.ts:225](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L225)
