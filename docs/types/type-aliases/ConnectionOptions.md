[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / ConnectionOptions

# Type Alias: ConnectionOptions

> **ConnectionOptions**: `Omit`\<[`TransactionOptions`](../interfaces/TransactionOptions.md), `"allowedAgentTypes"` \| `"allowedCounterpartyTypes"` \| `"vasps"` \| `"fields"` \| `"hide"`\>

An object representing options for a Connection Request

## Defined in

[types.ts:583](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L583)
