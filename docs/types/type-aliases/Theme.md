[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / Theme

# Type Alias: Theme

> **Theme**: `object`

The theme of the Notabene SDK

## Type declaration

### fontFamily?

> `optional` **fontFamily**: `string`

### logo?

> `optional` **logo**: `string`

### mode

> **mode**: `"light"` \| `"dark"`

### primaryColor?

> `optional` **primaryColor**: `string`

### secondaryColor?

> `optional` **secondaryColor**: `string`

## Defined in

[types.ts:237](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L237)
