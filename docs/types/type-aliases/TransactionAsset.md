[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / TransactionAsset

# Type Alias: TransactionAsset

> **TransactionAsset**: [`NotabeneAsset`](NotabeneAsset.md) \| [`CAIP19`](CAIP19.md) \| [`DTI`](DTI.md)

The asset of a transaction either a Notabene asset, a CAIP-19 asset, or a DTI.

## Defined in

[types.ts:145](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L145)
