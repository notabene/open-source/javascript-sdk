[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / Error

# Type Alias: Error

> **Error**: `object`

Represents an error component message

## Type declaration

### description?

> `optional` **description**: `string`

### identifier?

> `optional` **identifier**: [`ErrorIdentifierCode`](../enumerations/ErrorIdentifierCode.md)

### message

> **message**: `string`

### type

> **type**: [`ERROR`](../enumerations/CMType.md#error)

## Param

Error message

## Param

Description of the error message

## Param

Identifier code of the error message

## Defined in

[types.ts:858](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L858)
