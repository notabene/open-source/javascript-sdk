[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / LegalPersonFields

# Type Alias: LegalPersonFields

> **LegalPersonFields**: `{ [name in LegalPersonFieldName]?: FieldOptions }`

Field properties by field name

## Defined in

[types.ts:444](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L444)
