[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / Completed

# Type Alias: Completed\<T\>

> **Completed**\<`T`\>: `object`

Represents a completed component message

## Type Parameters

• **T**

The overall Value type being returned

## Type declaration

### response

> **response**: [`TransactionResponse`](../interfaces/TransactionResponse.md)\<`T`\>

### type

> **type**: [`COMPLETE`](../enumerations/CMType.md#complete)

## Param

The Response object which wraps T

## Defined in

[types.ts:818](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L818)
