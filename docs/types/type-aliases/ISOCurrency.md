[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / ISOCurrency

# Type Alias: ISOCurrency

> **ISOCurrency**: `string`

3 letter ISO currency code

## Defined in

[types.ts:231](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L231)
