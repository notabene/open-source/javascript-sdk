[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / Source

# Type Alias: Source

> **Source**: [`BlockchainAddress`](BlockchainAddress.md) \| [`CAIP10`](CAIP10.md)

The source of a transaction

## Defined in

[types.ts:207](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L207)
