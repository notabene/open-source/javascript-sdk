[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / Ready

# Type Alias: Ready

> **Ready**: `object`

Represents a ready component message

## Type declaration

### type

> **type**: [`READY`](../enumerations/CMType.md#ready)

## Defined in

[types.ts:827](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L827)
