[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / FieldTypes

# Type Alias: FieldTypes

> **FieldTypes**: `object`

Field type configuration

## Type declaration

### legalPerson?

> `optional` **legalPerson**: [`LegalPersonFields`](LegalPersonFields.md)

### naturalPerson?

> `optional` **naturalPerson**: [`NaturalPersonFields`](NaturalPersonFields.md)

## Defined in

[types.ts:711](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L711)
