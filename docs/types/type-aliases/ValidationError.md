[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / ValidationError

# Type Alias: ValidationError

> **ValidationError**: `object`

Validation error

## Type declaration

### attribute

> **attribute**: `string`

### message

> **message**: `string`

## Defined in

[types.ts:690](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L690)
