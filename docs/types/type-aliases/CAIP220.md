[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / CAIP220

# Type Alias: CAIP220

> **CAIP220**: `string`

Chain Agnostic Payload Identifier

## Defined in

[types.ts:97](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L97)
