[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / BlockchainAddress

# Type Alias: BlockchainAddress

> **BlockchainAddress**: `string`

A native blockchain address string

## Remarks

Represents a blockchain address in the native format specific to a particular chain.
This could be an Ethereum address, Bitcoin address, or other chain-specific format.
The address format and validation rules depend on the underlying blockchain.

## Examples

```ts
"0x742d35Cc6634C0532925a3b844Bc454e4438f44e" // Ethereum address
```

```ts
"1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa" // Bitcoin address
```

```ts
"cosmos1t2uflqwqe0fsj0shcfkrvpukewcw40yjj6hdc0" // Cosmos address
@public
```

## Defined in

[types.ts:166](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L166)
