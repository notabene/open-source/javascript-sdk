[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / NaturalPersonFieldName

# Type Alias: NaturalPersonFieldName

> **NaturalPersonFieldName**: `"name"` \| `"website"` \| `"email"` \| `"phone"` \| `"geographicAddress"` \| `"nationalIdentification"` \| `"dateOfBirth"` \| `"placeOfBirth"` \| `"countryOfResidence"`

Field names for NaturalPerson

## Defined in

[types.ts:383](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L383)
