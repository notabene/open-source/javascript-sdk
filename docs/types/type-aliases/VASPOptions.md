[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / VASPOptions

# Type Alias: VASPOptions

> **VASPOptions**: `object`

Options for which VASPs to be searchable

## Type declaration

### addUnknown?

> `optional` **addUnknown**: `boolean`

### onlyActive?

> `optional` **onlyActive**: `boolean`

### searchable?

> `optional` **searchable**: [`VASPSearchControl`](../enumerations/VASPSearchControl.md)[]

## Defined in

[types.ts:733](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L733)
