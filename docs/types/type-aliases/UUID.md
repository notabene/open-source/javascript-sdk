[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / UUID

# Type Alias: UUID

> **UUID**: `string`

UUID v4 string identifier
A universally unique identifier that follows RFC 4122 format
Format: 8-4-4-4-12 hexadecimal digits

## Example

```ts
"550e8400-e29b-41d4-a716-446655440000"
```

## See

[RFC4122](https://tools.ietf.org/html/rfc4122)

## Defined in

[types.ts:39](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L39)
