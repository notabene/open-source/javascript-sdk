[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / ComponentMessage

# Type Alias: ComponentMessage\<T\>

> **ComponentMessage**\<`T`\>: [`Completed`](Completed.md)\<`T`\> \| [`Cancel`](Cancel.md) \| [`Error`](Error.md) \| [`Ready`](Ready.md) \| [`ResizeRequest`](ResizeRequest.md) \| [`InvalidValue`](InvalidValue.md)\<`T`\>

Union type representing all possible messages that can be sent from a component

## Type Parameters

• **T**

The value type that will be returned in Completed messages

## Remarks

Components communicate their state and results back to the host application
through these message types:
- Completed: Operation finished successfully with response data
- Cancel: User cancelled the operation
- Error: Operation failed with error message
- ResizeRequest: Component needs to adjust its dimensions
- InvalidValue: Validation failed with current partial value

## See

 - [Completed](Completed.md) For successful completion message format
 - [Cancel](Cancel.md) For cancellation message format
 - [Error](Error.md) For error message format
 - [ResizeRequest](ResizeRequest.md) For resize message format
 - [InvalidValue](InvalidValue.md) For validation failure message format

## Defined in

[types.ts:907](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L907)
