[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / VASPSearchControl

# Enumeration: VASPSearchControl

VASP Visibility Control representing allow/deny behaviour for VASP visibility

## Remarks

- ALLOWED: Include VASPs you have explicitly allowed in the Notabene Network
- PENDING: Include VASPs neither allowed nor denied

## Enumeration Members

### ALLOWED

> **ALLOWED**: `"allowed"`

#### Defined in

[types.ts:725](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L725)

***

### PENDING

> **PENDING**: `"pending"`

#### Defined in

[types.ts:726](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L726)
