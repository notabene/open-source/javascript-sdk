[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / Status

# Enumeration: Status

The verification status of a transaction

## Enumeration Members

### BANNED

> **BANNED**: `"banned"`

#### Defined in

[types.ts:597](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L597)

***

### EMPTY

> **EMPTY**: `"empty"`

#### Defined in

[types.ts:593](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L593)

***

### PENDING

> **PENDING**: `"pending"`

#### Defined in

[types.ts:595](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L595)

***

### VERIFIED

> **VERIFIED**: `"verified"`

#### Defined in

[types.ts:596](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L596)

***

### VERIFY

> **VERIFY**: `"verify"`

#### Defined in

[types.ts:594](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L594)
