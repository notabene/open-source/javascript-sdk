[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / AgentType

# Enumeration: AgentType

The type of Agent. Either a wallet or a VASP

## Enumeration Members

### PRIVATE

> **PRIVATE**: `"WALLET"`

#### Defined in

[types.ts:250](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L250)

***

### VASP

> **VASP**: `"VASP"`

#### Defined in

[types.ts:251](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L251)
