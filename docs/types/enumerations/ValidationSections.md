[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / ValidationSections

# Enumeration: ValidationSections

**`Alpha`**

Sections in a WithdrawalAssist screen

## Enumeration Members

### AGENT

> **AGENT**: `"agent"`

**`Alpha`**

#### Defined in

[types.ts:748](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L748)

***

### ASSET

> **ASSET**: `"asset"`

**`Alpha`**

#### Defined in

[types.ts:745](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L745)

***

### COUNTERPARTY

> **COUNTERPARTY**: `"counterparty"`

**`Alpha`**

#### Defined in

[types.ts:747](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L747)

***

### DESTINATION

> **DESTINATION**: `"destination"`

**`Alpha`**

#### Defined in

[types.ts:746](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L746)
