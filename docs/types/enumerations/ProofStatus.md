[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / ProofStatus

# Enumeration: ProofStatus

Status of the ownership proof verification process

## Remarks

Represents the different states that an ownership proof can be in during and after verification:
- PENDING: Initial state where verification is in progress or awaiting processing
- FAILED: The proof was rejected due to failing verification checks
- FLAGGED: The proof requires manual review due to suspicious or unclear verification results
- VERIFIED: The proof has passed all verification checks successfully

## Enumeration Members

### FAILED

> **FAILED**: `"rejected"`

#### Defined in

[types.ts:995](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L995)

***

### FLAGGED

> **FLAGGED**: `"flagged"`

#### Defined in

[types.ts:996](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L996)

***

### PENDING

> **PENDING**: `"pending"`

#### Defined in

[types.ts:994](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L994)

***

### VERIFIED

> **VERIFIED**: `"verified"`

#### Defined in

[types.ts:997](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L997)
