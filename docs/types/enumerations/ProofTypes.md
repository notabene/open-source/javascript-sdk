[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / ProofTypes

# Enumeration: ProofTypes

Types of ownership proofs supported by the system

## Remarks

Supported proof types:
- SelfDeclaration: User self-declares ownership without cryptographic proof
- EIP191: Ethereum personal signature following EIP-191 standard
- SIWE: Sign-In with Ethereum message signature (EIP-4361)
- EIP712: Ethereum typed data signature following EIP-712 standard
- BIP137: Bitcoin message signature following BIP-137
- BIP322: Bitcoin message signature following BIP-322
- TIP191: Tron message signing
- ED25519: Ed25519 signature (used in Solana)
- XPUB: Extended public key signature for HD wallets
- MicroTransfer: Proof via small blockchain transaction
- Screenshot: Image proof of ownership/access

## See

 - [SignatureProof](../interfaces/SignatureProof.md) For signature-based proofs
 - [DeclarationProof](../interfaces/DeclarationProof.md) For self-declaration proofs
 - [MicroTransferProof](../interfaces/MicroTransferProof.md) For transaction-based proofs
 - [ScreenshotProof](../interfaces/ScreenshotProof.md) For screenshot proofs

## Enumeration Members

### BIP137

> **BIP137**: `"bip-137"`

#### Defined in

[types.ts:1030](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1030)

***

### BIP137\_XPUB

> **BIP137\_XPUB**: `"xpub"`

#### Defined in

[types.ts:1032](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1032)

***

### BIP322

> **BIP322**: `"bip-322"`

#### Defined in

[types.ts:1031](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1031)

***

### Connect

> **Connect**: `"connect"`

#### Defined in

[types.ts:1037](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1037)

***

### ED25519

> **ED25519**: `"ed25519"`

#### Defined in

[types.ts:1034](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1034)

***

### EIP1271

> **EIP1271**: `"eip-1271"`

#### Defined in

[types.ts:1029](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1029)

***

### EIP191

> **EIP191**: `"eip-191"`

#### Defined in

[types.ts:1027](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1027)

***

### EIP712

> **EIP712**: `"eip-712"`

#### Defined in

[types.ts:1028](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1028)

***

### MicroTransfer

> **MicroTransfer**: `"microtransfer"`

#### Defined in

[types.ts:1035](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1035)

***

### Screenshot

> **Screenshot**: `"screenshot"`

#### Defined in

[types.ts:1036](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1036)

***

### SelfDeclaration

> **SelfDeclaration**: `"self-declaration"`

#### Defined in

[types.ts:1024](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1024)

***

### SIWE

> **SIWE**: `"siwe"`

#### Defined in

[types.ts:1025](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1025)

***

### SIWX

> **SIWX**: `"siwx"`

#### Defined in

[types.ts:1026](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1026)

***

### TIP191

> **TIP191**: `"tip-191"`

#### Defined in

[types.ts:1033](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1033)
