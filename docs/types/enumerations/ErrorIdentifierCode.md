[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / ErrorIdentifierCode

# Enumeration: ErrorIdentifierCode

Represents an error identifier code

## Enumeration Members

### SERVICE\_UNAVAILABLE

> **SERVICE\_UNAVAILABLE**: `"SERVICE_UNAVAILABLE"`

#### Defined in

[types.ts:845](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L845)

***

### TOKEN\_INVALID

> **TOKEN\_INVALID**: `"TOKEN_INVALID"`

#### Defined in

[types.ts:848](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L848)

***

### WALLET\_CONNECTION\_FAILED

> **WALLET\_CONNECTION\_FAILED**: `"WALLET_CONNECTION_FAILED"`

#### Defined in

[types.ts:846](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L846)

***

### WALLET\_NOT\_SUPPORTED

> **WALLET\_NOT\_SUPPORTED**: `"WALLET_NOT_SUPPORTED"`

#### Defined in

[types.ts:847](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L847)
