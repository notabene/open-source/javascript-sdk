[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / HMType

# Enumeration: HMType

Host Message Type enum representing different message types that can be sent
from the host application.

## Remarks

- UPDATE: Message to update component value/state
- REQUEST_RESPONSE: Message requesting a response from component

## Enumeration Members

### REQUEST\_RESPONSE

> **REQUEST\_RESPONSE**: `"requestResponse"`

#### Defined in

[types.ts:926](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L926)

***

### UPDATE

> **UPDATE**: `"update"`

#### Defined in

[types.ts:925](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L925)
