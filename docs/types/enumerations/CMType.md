[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / CMType

# Enumeration: CMType

Component Message Type enum representing different message types that can be sent
between the host and component.

## Remarks

- COMPLETE: Indicates a completed operation with response data
- RESIZE: Request to adjust component size/dimensions
- RESULT: Operation result notification
- READY: Component is initialized and ready
- INVALID: Validation failed with errors
- ERROR: Operation encountered an error
- CANCEL: Operation was cancelled

## Enumeration Members

### CANCEL

> **CANCEL**: `"cancel"`

#### Defined in

[types.ts:809](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L809)

***

### COMPLETE

> **COMPLETE**: `"complete"`

#### Defined in

[types.ts:803](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L803)

***

### ERROR

> **ERROR**: `"error"`

#### Defined in

[types.ts:808](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L808)

***

### INVALID

> **INVALID**: `"invalid"`

#### Defined in

[types.ts:807](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L807)

***

### READY

> **READY**: `"ready"`

#### Defined in

[types.ts:806](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L806)

***

### RESIZE

> **RESIZE**: `"resize"`

#### Defined in

[types.ts:804](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L804)

***

### RESULT

> **RESULT**: `"result"`

#### Defined in

[types.ts:805](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L805)
