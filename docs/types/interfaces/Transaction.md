[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / Transaction

# Interface: Transaction

Core transaction interface representing a crypto asset transfer between parties

## Remarks

Extends ComponentRequest to add transaction-specific properties:
- agent: The entity facilitating/executing the transaction
- counterparty: The other party involved in the transaction
- asset: The cryptocurrency or token being transferred
- amountDecimal: The amount to transfer in decimal format
- proof: Optional ownership proof verifying control of involved addresses
- assetPrice: Optional price information in a fiat currency

This interface serves as the base for specific transaction types like:
- Withdrawals for sending assets out
- Deposits for receiving assets
- Deposit requests for requesting asset transfers

## See

 - [Withdrawal](Withdrawal.md) For withdrawal-specific transaction properties
 - [Deposit](Deposit.md) For deposit-specific transaction properties
 - [Agent](Agent.md) For agent details
 - [Counterparty](Counterparty.md) For counterparty information

## Extends

- [`ComponentRequest`](ComponentRequest.md)

## Extended by

- [`DepositTransaction`](DepositTransaction.md)
- [`Withdrawal`](Withdrawal.md)

## Properties

### agent

> **agent**: [`Agent`](Agent.md)

#### Defined in

[types.ts:523](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L523)

***

### amountDecimal

> **amountDecimal**: `number`

#### Defined in

[types.ts:526](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L526)

***

### asset

> **asset**: `string`

#### Defined in

[types.ts:525](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L525)

***

### assetPrice?

> `optional` **assetPrice**: `object`

#### currency

> **currency**: `string`

#### price

> **price**: `number`

#### Defined in

[types.ts:528](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L528)

***

### counterparty

> **counterparty**: [`Counterparty`](Counterparty.md)

#### Defined in

[types.ts:524](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L524)

***

### customer?

> `optional` **customer**: [`Counterparty`](Counterparty.md)

#### Inherited from

[`ComponentRequest`](ComponentRequest.md).[`customer`](ComponentRequest.md#customer)

#### Defined in

[types.ts:496](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L496)

***

### proof?

> `optional` **proof**: [`OwnershipProof`](OwnershipProof.md)

#### Defined in

[types.ts:527](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L527)

***

### requestId?

> `optional` **requestId**: `string`

#### Inherited from

[`ComponentRequest`](ComponentRequest.md).[`requestId`](ComponentRequest.md#requestid)

#### Defined in

[types.ts:495](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L495)
