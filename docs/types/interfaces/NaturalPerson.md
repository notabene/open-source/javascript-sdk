[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / NaturalPerson

# Interface: NaturalPerson

Interface representing a natural person (individual) involved in a transaction

## Remarks

Extends the baseinterface to add properties specific to individual persons:
- type: Must be PersonType.NATURAL to identify as an individual
- dateOfBirth: Optional ISO format birth date for identity verification
- placeOfBirth: Optional birth place for identity verification
- countryOfResidence: Optional ISO country code of current residence
- name: Required full legal name of the individual

This interface captures the additional identifying information required for
natural persons under FATF Travel Rule requirements. The properties align
with standard KYC (Know Your Customer) data collection practices.

## See

 - [Counterparty](Counterparty.md) For base properties common to all counterparties
 - [PersonType](../enumerations/PersonType.md) For person type classification

## Extends

- [`Counterparty`](Counterparty.md)

## Properties

### accountNumber?

> `optional` **accountNumber**: `string`

#### Inherited from

[`Counterparty`](Counterparty.md).[`accountNumber`](Counterparty.md#accountnumber)

#### Defined in

[types.ts:342](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L342)

***

### countryOfResidence?

> `optional` **countryOfResidence**: `string`

#### Defined in

[types.ts:376](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L376)

***

### dateOfBirth?

> `optional` **dateOfBirth**: \`$\{number\}-$\{number\}-$\{number\}\`

#### Defined in

[types.ts:374](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L374)

***

### did?

> `optional` **did**: \`did:$\{string\}:$\{string\}\`

#### Inherited from

[`Counterparty`](Counterparty.md).[`did`](Counterparty.md#did)

#### Defined in

[types.ts:343](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L343)

***

### email?

> `optional` **email**: `string`

#### Inherited from

[`Counterparty`](Counterparty.md).[`email`](Counterparty.md#email)

#### Defined in

[types.ts:350](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L350)

***

### geographicAddress?

> `optional` **geographicAddress**: [`Address`](../../ivms/types/type-aliases/Address.md)

#### Inherited from

[`Counterparty`](Counterparty.md).[`geographicAddress`](Counterparty.md#geographicaddress)

#### Defined in

[types.ts:346](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L346)

***

### name

> **name**: `string`

#### Overrides

[`Counterparty`](Counterparty.md).[`name`](Counterparty.md#name)

#### Defined in

[types.ts:377](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L377)

***

### nationalIdentification?

> `optional` **nationalIdentification**: [`NationalIdentification`](../../ivms/types/type-aliases/NationalIdentification.md)

#### Inherited from

[`Counterparty`](Counterparty.md).[`nationalIdentification`](Counterparty.md#nationalidentification)

#### Defined in

[types.ts:347](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L347)

***

### phone?

> `optional` **phone**: `string`

#### Inherited from

[`Counterparty`](Counterparty.md).[`phone`](Counterparty.md#phone)

#### Defined in

[types.ts:349](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L349)

***

### placeOfBirth?

> `optional` **placeOfBirth**: `string`

#### Defined in

[types.ts:375](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L375)

***

### type

> **type**: [`NATURAL`](../enumerations/PersonType.md#natural)

#### Overrides

[`Counterparty`](Counterparty.md).[`type`](Counterparty.md#type)

#### Defined in

[types.ts:373](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L373)

***

### verified?

> `optional` **verified**: `boolean`

#### Inherited from

[`Counterparty`](Counterparty.md).[`verified`](Counterparty.md#verified)

#### Defined in

[types.ts:345](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L345)

***

### website?

> `optional` **website**: `string`

#### Inherited from

[`Counterparty`](Counterparty.md).[`website`](Counterparty.md#website)

#### Defined in

[types.ts:348](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L348)
