[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / ComponentResponse

# Interface: ComponentResponse

Base response interface for all SDK component operations

## Remarks

Provides standardized response propertiesfor component interactions:
- requestID: Links response back to the originating request
- valid: Boolean indicating if the operation was valid/successful
- status: Current verification status of the operation
- errors: Array of validation errors if any occurred

This interface is extended by specific response types like:
- TransResponse for transaction operations
- ConnectionResponse for VASP connection operations

## See

 - [Status](../enumerations/Status.md) For possible status values
 - [ValidationError](../type-aliases/ValidationError.md) For error structure
 - [TransactionResponse](TransactionResponse.md) For transaction-specific responses

## Extended by

- [`TransactionResponse`](TransactionResponse.md)

## Properties

### errors

> **errors**: [`ValidationError`](../type-aliases/ValidationError.md)[]

#### Defined in

[types.ts:656](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L656)

***

### requestID

> **requestID**: `string`

#### Defined in

[types.ts:653](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L653)

***

### status

> **status**: [`Status`](../enumerations/Status.md)

#### Defined in

[types.ts:655](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L655)

***

### valid

> **valid**: `boolean`

#### Defined in

[types.ts:654](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L654)
