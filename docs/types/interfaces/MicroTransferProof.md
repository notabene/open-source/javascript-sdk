[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / MicroTransferProof

# Interface: MicroTransferProof

Ownership Proof using Micro Transfer

## Extends

- [`OwnershipProof`](OwnershipProof.md)

## Properties

### address

> **address**: \`$\{string\}:$\{string\}:$\{string\}\`

#### Inherited from

[`OwnershipProof`](OwnershipProof.md).[`address`](OwnershipProof.md#address)

#### Defined in

[types.ts:1065](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1065)

***

### amountSubunits

> **amountSubunits**: `string`

#### Defined in

[types.ts:1144](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1144)

***

### asset

> **asset**: \`$\{string\}:$\{string\}/$\{string\}:$\{string\}\`

#### Defined in

[types.ts:1142](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1142)

***

### chain

> **chain**: \`$\{string\}:$\{string\}\`

#### Defined in

[types.ts:1141](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1141)

***

### destination

> **destination**: `string`

#### Defined in

[types.ts:1143](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1143)

***

### did

> **did**: \`did:$\{string\}:$\{string\}\`

#### Inherited from

[`OwnershipProof`](OwnershipProof.md).[`did`](OwnershipProof.md#did)

#### Defined in

[types.ts:1064](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1064)

***

### proof

> **proof**: `string`

#### Defined in

[types.ts:1140](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1140)

***

### status

> **status**: [`ProofStatus`](../enumerations/ProofStatus.md)

#### Inherited from

[`OwnershipProof`](OwnershipProof.md).[`status`](OwnershipProof.md#status)

#### Defined in

[types.ts:1063](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1063)

***

### type

> **type**: [`MicroTransfer`](../enumerations/ProofTypes.md#microtransfer)

#### Overrides

[`OwnershipProof`](OwnershipProof.md).[`type`](OwnershipProof.md#type)

#### Defined in

[types.ts:1139](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1139)
