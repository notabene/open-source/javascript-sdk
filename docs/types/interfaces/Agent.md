[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / Agent

# Interface: Agent

Who is the agent acting on behalf of the counterparty

## Extended by

- [`VASP`](VASP.md)
- [`Wallet`](Wallet.md)

## Properties

### did

> **did**: \`did:$\{string\}:$\{string\}\`

#### Defined in

[types.ts:259](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L259)

***

### logo?

> `optional` **logo**: `string`

#### Defined in

[types.ts:261](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L261)

***

### name?

> `optional` **name**: `string`

#### Defined in

[types.ts:263](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L263)

***

### type

> **type**: [`AgentType`](../enumerations/AgentType.md)

#### Defined in

[types.ts:260](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L260)

***

### url?

> `optional` **url**: `string`

#### Defined in

[types.ts:262](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L262)

***

### verified?

> `optional` **verified**: `boolean`

#### Defined in

[types.ts:264](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L264)
