[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / ScreenshotProof

# Interface: ScreenshotProof

Ownership Proof using Screenshot

## Extends

- [`OwnershipProof`](OwnershipProof.md)

## Properties

### address

> **address**: \`$\{string\}:$\{string\}:$\{string\}\`

#### Inherited from

[`OwnershipProof`](OwnershipProof.md).[`address`](OwnershipProof.md#address)

#### Defined in

[types.ts:1065](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1065)

***

### did

> **did**: \`did:$\{string\}:$\{string\}\`

#### Inherited from

[`OwnershipProof`](OwnershipProof.md).[`did`](OwnershipProof.md#did)

#### Defined in

[types.ts:1064](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1064)

***

### status

> **status**: [`ProofStatus`](../enumerations/ProofStatus.md)

#### Inherited from

[`OwnershipProof`](OwnershipProof.md).[`status`](OwnershipProof.md#status)

#### Defined in

[types.ts:1063](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1063)

***

### type

> **type**: [`Screenshot`](../enumerations/ProofTypes.md#screenshot)

#### Overrides

[`OwnershipProof`](OwnershipProof.md).[`type`](OwnershipProof.md#type)

#### Defined in

[types.ts:1152](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1152)

***

### url

> **url**: `string`

#### Defined in

[types.ts:1153](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1153)
