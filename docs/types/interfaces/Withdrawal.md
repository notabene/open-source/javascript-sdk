[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / Withdrawal

# Interface: Withdrawal

An object representing a withdrawal transaction

## Extends

- `BeneficiaryFields`.[`Transaction`](Transaction.md)

## Properties

### agent

> **agent**: [`Agent`](Agent.md)

#### Inherited from

[`Transaction`](Transaction.md).[`agent`](Transaction.md#agent)

#### Defined in

[types.ts:523](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L523)

***

### amountDecimal

> **amountDecimal**: `number`

#### Inherited from

[`Transaction`](Transaction.md).[`amountDecimal`](Transaction.md#amountdecimal)

#### Defined in

[types.ts:526](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L526)

***

### asset

> **asset**: `string`

#### Inherited from

[`Transaction`](Transaction.md).[`asset`](Transaction.md#asset)

#### Defined in

[types.ts:525](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L525)

***

### assetPrice?

> `optional` **assetPrice**: `object`

#### currency

> **currency**: `string`

#### price

> **price**: `number`

#### Inherited from

[`Transaction`](Transaction.md).[`assetPrice`](Transaction.md#assetprice)

#### Defined in

[types.ts:528](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L528)

***

### counterparty

> **counterparty**: [`Counterparty`](Counterparty.md)

#### Inherited from

[`Transaction`](Transaction.md).[`counterparty`](Transaction.md#counterparty)

#### Defined in

[types.ts:524](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L524)

***

### customer?

> `optional` **customer**: [`Counterparty`](Counterparty.md)

#### Inherited from

[`Transaction`](Transaction.md).[`customer`](Transaction.md#customer)

#### Defined in

[types.ts:496](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L496)

***

### destination?

> `optional` **destination**: `string`

#### Inherited from

`BeneficiaryFields.destination`

#### Defined in

[types.ts:461](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L461)

***

### proof?

> `optional` **proof**: [`OwnershipProof`](OwnershipProof.md)

#### Inherited from

[`Transaction`](Transaction.md).[`proof`](Transaction.md#proof)

#### Defined in

[types.ts:527](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L527)

***

### requestId?

> `optional` **requestId**: `string`

#### Inherited from

[`Transaction`](Transaction.md).[`requestId`](Transaction.md#requestid)

#### Defined in

[types.ts:495](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L495)
