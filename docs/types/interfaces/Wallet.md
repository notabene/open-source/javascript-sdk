[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / Wallet

# Interface: Wallet

A wallet agent acting on behalf of the counterparty

## Extends

- [`Agent`](Agent.md)

## Properties

### did

> **did**: \`did:$\{string\}:$\{string\}\`

#### Inherited from

[`Agent`](Agent.md).[`did`](Agent.md#did)

#### Defined in

[types.ts:259](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L259)

***

### logo?

> `optional` **logo**: `string`

#### Inherited from

[`Agent`](Agent.md).[`logo`](Agent.md#logo)

#### Defined in

[types.ts:261](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L261)

***

### name?

> `optional` **name**: `string`

#### Inherited from

[`Agent`](Agent.md).[`name`](Agent.md#name)

#### Defined in

[types.ts:263](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L263)

***

### proof

> **proof**: [`OwnershipProof`](OwnershipProof.md)

#### Defined in

[types.ts:310](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L310)

***

### type

> **type**: [`AgentType`](../enumerations/AgentType.md)

#### Inherited from

[`Agent`](Agent.md).[`type`](Agent.md#type)

#### Defined in

[types.ts:260](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L260)

***

### url?

> `optional` **url**: `string`

#### Inherited from

[`Agent`](Agent.md).[`url`](Agent.md#url)

#### Defined in

[types.ts:262](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L262)

***

### verified?

> `optional` **verified**: `boolean`

#### Inherited from

[`Agent`](Agent.md).[`verified`](Agent.md#verified)

#### Defined in

[types.ts:264](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L264)

***

### wallet\_connect\_id?

> `optional` **wallet\_connect\_id**: `string`

#### Defined in

[types.ts:311](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L311)
