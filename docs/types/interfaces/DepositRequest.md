[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / DepositRequest

# Interface: DepositRequest

An object representing a request for a deposit

## Extends

- `DepositRequestFields`.[`ComponentRequest`](ComponentRequest.md)

## Properties

### amountDecimal?

> `optional` **amountDecimal**: `number`

#### Inherited from

`DepositRequestFields.amountDecimal`

#### Defined in

[types.ts:471](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L471)

***

### asset

> **asset**: `string`

#### Inherited from

`DepositRequestFields.asset`

#### Defined in

[types.ts:470](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L470)

***

### cryptoCredential?

> `optional` **cryptoCredential**: \`$\{string\}.$\{string\}.mastercard\`

#### Inherited from

`DepositRequestFields.cryptoCredential`

#### Defined in

[types.ts:473](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L473)

***

### customer?

> `optional` **customer**: [`Counterparty`](Counterparty.md)

#### Inherited from

[`ComponentRequest`](ComponentRequest.md).[`customer`](ComponentRequest.md#customer)

#### Defined in

[types.ts:496](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L496)

***

### destination

> **destination**: `string`

#### Inherited from

`DepositRequestFields.destination`

#### Defined in

[types.ts:469](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L469)

***

### requestId?

> `optional` **requestId**: `string`

#### Inherited from

[`ComponentRequest`](ComponentRequest.md).[`requestId`](ComponentRequest.md#requestid)

#### Defined in

[types.ts:495](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L495)

***

### travelAddress?

> `optional` **travelAddress**: \`ta$\{string\}\`

#### Inherited from

`DepositRequestFields.travelAddress`

#### Defined in

[types.ts:472](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L472)
