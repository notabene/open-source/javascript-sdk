[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / DepositRequestOptions

# Interface: DepositRequestOptions

An object representing options for a Deposit Request

## Properties

### showQrCode?

> `optional` **showQrCode**: `boolean`

#### Defined in

[types.ts:567](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L567)
