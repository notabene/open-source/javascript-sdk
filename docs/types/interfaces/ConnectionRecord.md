[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / ConnectionRecord

# Interface: ConnectionRecord

Interface for recording that user connected their wallet.

## Remarks

- Records which wallet provider was used to connect

The signature proves ownership by demonstrating control of the private keys
associated with the claimed address.

## See

 - [ProofTypes](../enumerations/ProofTypes.md) For supported signature types
 - [OwnershipProof](OwnershipProof.md) For base proof properties

## Extends

- [`OwnershipProof`](OwnershipProof.md)

## Properties

### address

> **address**: \`$\{string\}:$\{string\}:$\{string\}\`

#### Inherited from

[`OwnershipProof`](OwnershipProof.md).[`address`](OwnershipProof.md#address)

#### Defined in

[types.ts:1065](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1065)

***

### attestation

> **attestation**: `string`

#### Defined in

[types.ts:1130](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1130)

***

### did

> **did**: \`did:$\{string\}:$\{string\}\`

#### Inherited from

[`OwnershipProof`](OwnershipProof.md).[`did`](OwnershipProof.md#did)

#### Defined in

[types.ts:1064](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1064)

***

### proof

> **proof**: `string`

#### Defined in

[types.ts:1129](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1129)

***

### status

> **status**: [`ProofStatus`](../enumerations/ProofStatus.md)

#### Inherited from

[`OwnershipProof`](OwnershipProof.md).[`status`](OwnershipProof.md#status)

#### Defined in

[types.ts:1063](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1063)

***

### type

> **type**: [`Connect`](../enumerations/ProofTypes.md#connect)

#### Overrides

[`OwnershipProof`](OwnershipProof.md).[`type`](OwnershipProof.md#type)

#### Defined in

[types.ts:1128](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1128)

***

### wallet\_provider

> **wallet\_provider**: `string`

#### Defined in

[types.ts:1131](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1131)
