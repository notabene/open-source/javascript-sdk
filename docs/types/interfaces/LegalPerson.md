[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / LegalPerson

# Interface: LegalPerson

Interface representing a legal entity (organization/company) involved in a transaction

## Remarks

Extends the baseface to add properties specific to legal entities:
- type: MustPersonType.LEGAL to identify as an organization
- name: Required registered legal name of the entity
- lei: Optional Legal Entity Identifier for regulated entities
- logo: Optional URI to the organization's logo image
- countryOfRegistration: Optional ISO country code where entity is registered

This interface captures the additional identifying information required for
legal persons under FATF Travel Rule requirements. The properties align with
standard business KYC (Know Your Businessta collection practices.

## See

 - [Counterparty](Counterparty.md) For base properties common to all counterparties
 - [PersonType](../enumerations/PersonType.md) For person type classification
 - [LEI](../type-aliases/LEI.md) For Legal Entity Identifier format

## Extends

- [`Counterparty`](Counterparty.md)

## Properties

### accountNumber?

> `optional` **accountNumber**: `string`

#### Inherited from

[`Counterparty`](Counterparty.md).[`accountNumber`](Counterparty.md#accountnumber)

#### Defined in

[types.ts:342](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L342)

***

### countryOfRegistration?

> `optional` **countryOfRegistration**: `string`

#### Defined in

[types.ts:427](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L427)

***

### did?

> `optional` **did**: \`did:$\{string\}:$\{string\}\`

#### Inherited from

[`Counterparty`](Counterparty.md).[`did`](Counterparty.md#did)

#### Defined in

[types.ts:343](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L343)

***

### email?

> `optional` **email**: `string`

#### Inherited from

[`Counterparty`](Counterparty.md).[`email`](Counterparty.md#email)

#### Defined in

[types.ts:350](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L350)

***

### geographicAddress?

> `optional` **geographicAddress**: [`Address`](../../ivms/types/type-aliases/Address.md)

#### Inherited from

[`Counterparty`](Counterparty.md).[`geographicAddress`](Counterparty.md#geographicaddress)

#### Defined in

[types.ts:346](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L346)

***

### lei?

> `optional` **lei**: `string`

#### Defined in

[types.ts:425](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L425)

***

### logo?

> `optional` **logo**: `string`

#### Defined in

[types.ts:426](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L426)

***

### name

> **name**: `string`

#### Overrides

[`Counterparty`](Counterparty.md).[`name`](Counterparty.md#name)

#### Defined in

[types.ts:424](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L424)

***

### nationalIdentification?

> `optional` **nationalIdentification**: [`NationalIdentification`](../../ivms/types/type-aliases/NationalIdentification.md)

#### Inherited from

[`Counterparty`](Counterparty.md).[`nationalIdentification`](Counterparty.md#nationalidentification)

#### Defined in

[types.ts:347](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L347)

***

### phone?

> `optional` **phone**: `string`

#### Inherited from

[`Counterparty`](Counterparty.md).[`phone`](Counterparty.md#phone)

#### Defined in

[types.ts:349](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L349)

***

### type

> **type**: [`LEGAL`](../enumerations/PersonType.md#legal)

#### Overrides

[`Counterparty`](Counterparty.md).[`type`](Counterparty.md#type)

#### Defined in

[types.ts:423](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L423)

***

### verified?

> `optional` **verified**: `boolean`

#### Inherited from

[`Counterparty`](Counterparty.md).[`verified`](Counterparty.md#verified)

#### Defined in

[types.ts:345](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L345)

***

### website?

> `optional` **website**: `string`

#### Inherited from

[`Counterparty`](Counterparty.md).[`website`](Counterparty.md#website)

#### Defined in

[types.ts:348](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L348)
