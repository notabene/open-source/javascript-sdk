[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / ThresholdOptions

# Interface: ThresholdOptions

Specify what to do under the provided threshold.

Eg. to allow self-declaration for all transactions under 1000 EUR

Note to support threshold you MUST include the Asset Price in the Transaction

## See

[Transaction](Transaction.md) Transaction object

## Properties

### currency

> **currency**: `string`

#### Defined in

[types.ts:764](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L764)

***

### proofTypes?

> `optional` **proofTypes**: [`ProofTypes`](../enumerations/ProofTypes.md)[]

#### Defined in

[types.ts:765](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L765)

***

### threshold

> **threshold**: `number`

#### Defined in

[types.ts:763](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L763)
