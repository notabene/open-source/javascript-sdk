[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / TransactionResponse

# Interface: TransactionResponse\<V\>

Response interface for transaction-related operations

## Remarks

Extends ComponentResponse to add transaction-specific response data:
- value: The resulting transaction value of generic type V
- ivms101: IVMS 101 travel rule data for the transaction
- proof: Optional ownership proof details if required
- txCreate: Optional V1 transaction payload for legacy API compatibility
- txUpdate: Optional V1 transaction payload for legacy API compatibility

## See

 - [ComponentResponse](ComponentResponse.md) For base response properties
 - [IVMS101](../../ivms/types/type-aliases/IVMS101.md) For travel rule data structure
 - [OwnershipProof](OwnershipProof.md) For proof details
 - [V1Transaction](../type-aliases/V1Transaction.md) For legacy transaction format

## Extends

- [`ComponentResponse`](ComponentResponse.md)

## Type Parameters

• **V**

Type of the transaction value being returned

## Properties

### errors

> **errors**: [`ValidationError`](../type-aliases/ValidationError.md)[]

#### Inherited from

[`ComponentResponse`](ComponentResponse.md).[`errors`](ComponentResponse.md#errors)

#### Defined in

[types.ts:656](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L656)

***

### ivms101

> **ivms101**: [`IVMS101`](../../ivms/types/type-aliases/IVMS101.md)

#### Defined in

[types.ts:680](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L680)

***

### proof?

> `optional` **proof**: [`OwnershipProof`](OwnershipProof.md)

#### Defined in

[types.ts:681](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L681)

***

### requestID

> **requestID**: `string`

#### Inherited from

[`ComponentResponse`](ComponentResponse.md).[`requestID`](ComponentResponse.md#requestid)

#### Defined in

[types.ts:653](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L653)

***

### status

> **status**: [`Status`](../enumerations/Status.md)

#### Inherited from

[`ComponentResponse`](ComponentResponse.md).[`status`](ComponentResponse.md#status)

#### Defined in

[types.ts:655](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L655)

***

### txCreate?

> `optional` **txCreate**: [`V1Transaction`](../type-aliases/V1Transaction.md)

#### Defined in

[types.ts:682](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L682)

***

### txUpdate?

> `optional` **txUpdate**: [`V1Transaction`](../type-aliases/V1Transaction.md)

#### Defined in

[types.ts:683](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L683)

***

### valid

> **valid**: `boolean`

#### Inherited from

[`ComponentResponse`](ComponentResponse.md).[`valid`](ComponentResponse.md#valid)

#### Defined in

[types.ts:654](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L654)

***

### value

> **value**: `V`

#### Defined in

[types.ts:679](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L679)
