[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / Deposit

# Interface: Deposit

An object representing a deposit transaction

## Extends

- `OriginatorFields`.[`DepositTransaction`](DepositTransaction.md)

## Properties

### agent

> **agent**: [`Agent`](Agent.md)

#### Inherited from

[`DepositTransaction`](DepositTransaction.md).[`agent`](DepositTransaction.md#agent)

#### Defined in

[types.ts:523](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L523)

***

### amountDecimal

> **amountDecimal**: `number`

#### Inherited from

[`DepositTransaction`](DepositTransaction.md).[`amountDecimal`](DepositTransaction.md#amountdecimal)

#### Defined in

[types.ts:526](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L526)

***

### asset

> **asset**: `string`

#### Inherited from

[`DepositTransaction`](DepositTransaction.md).[`asset`](DepositTransaction.md#asset)

#### Defined in

[types.ts:525](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L525)

***

### assetPrice?

> `optional` **assetPrice**: `object`

#### currency

> **currency**: `string`

#### price

> **price**: `number`

#### Inherited from

[`DepositTransaction`](DepositTransaction.md).[`assetPrice`](DepositTransaction.md#assetprice)

#### Defined in

[types.ts:528](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L528)

***

### counterparty

> **counterparty**: [`Counterparty`](Counterparty.md)

#### Inherited from

[`DepositTransaction`](DepositTransaction.md).[`counterparty`](DepositTransaction.md#counterparty)

#### Defined in

[types.ts:524](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L524)

***

### customer?

> `optional` **customer**: [`Counterparty`](Counterparty.md)

#### Inherited from

[`DepositTransaction`](DepositTransaction.md).[`customer`](DepositTransaction.md#customer)

#### Defined in

[types.ts:496](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L496)

***

### proof?

> `optional` **proof**: [`OwnershipProof`](OwnershipProof.md)

#### Inherited from

[`DepositTransaction`](DepositTransaction.md).[`proof`](DepositTransaction.md#proof)

#### Defined in

[types.ts:527](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L527)

***

### requestId?

> `optional` **requestId**: `string`

#### Inherited from

[`DepositTransaction`](DepositTransaction.md).[`requestId`](DepositTransaction.md#requestid)

#### Defined in

[types.ts:495](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L495)

***

### source?

> `optional` **source**: `string`

#### Inherited from

`OriginatorFields.source`

#### Defined in

[types.ts:453](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L453)

***

### transactionId?

> `optional` **transactionId**: `string`

#### Inherited from

[`DepositTransaction`](DepositTransaction.md).[`transactionId`](DepositTransaction.md#transactionid)

#### Defined in

[types.ts:539](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L539)
