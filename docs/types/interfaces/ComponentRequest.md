[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / ComponentRequest

# Interface: ComponentRequest

Base interface for requests sent to SDK components

## Remarks

Defines core properties that all component requests share:
- Optional unique request ID for tracking/correlating requests and responses
- Optional customer detailsfor pre-filling component data

This interface is extended by specific request types like:
- Transaction requests for sending/receiving assets
- Connection requests for establishing VASP to VASP communication

## See

 - [Transaction](Transaction.md) For transaction-specific request properties
 - [ConnectionRequest](ConnectionRequest.md) For connection-specific request properties

## Extended by

- [`Transaction`](Transaction.md)
- [`DepositRequest`](DepositRequest.md)
- [`ConnectionRequest`](ConnectionRequest.md)

## Properties

### customer?

> `optional` **customer**: [`Counterparty`](Counterparty.md)

#### Defined in

[types.ts:496](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L496)

***

### requestId?

> `optional` **requestId**: `string`

#### Defined in

[types.ts:495](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L495)
