[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / Counterparty

# Interface: Counterparty

Interface representing a party involved in a transaction other than the initiator

## Remarks

fines the core properties that identify and describe a counterparty:
- name: The display or legal name of the counterparty
- accountNumber: An account identifier/reference number
- did: Decentralized identifier for the counterparty
- type: Classification as natural person, legal entity, or self
- verified: Whether the counterparty's identity has been verified
- geographicAddress: Physical/mailing address information
- nationalIdentification: Government-issued ID details
- website: Official web presence
- phone: Contact phone number
- email: Contact email address

This interface serves as the base for more specific counterparty types:

## See

 - [NaturalPerson](NaturalPerson.md) For individual person properties
 - [LegalPerson](LegalPerson.md) For organization/entity properties

## Extended by

- [`NaturalPerson`](NaturalPerson.md)
- [`LegalPerson`](LegalPerson.md)

## Properties

### accountNumber?

> `optional` **accountNumber**: `string`

#### Defined in

[types.ts:342](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L342)

***

### did?

> `optional` **did**: \`did:$\{string\}:$\{string\}\`

#### Defined in

[types.ts:343](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L343)

***

### email?

> `optional` **email**: `string`

#### Defined in

[types.ts:350](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L350)

***

### geographicAddress?

> `optional` **geographicAddress**: [`Address`](../../ivms/types/type-aliases/Address.md)

#### Defined in

[types.ts:346](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L346)

***

### name?

> `optional` **name**: `string`

#### Defined in

[types.ts:341](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L341)

***

### nationalIdentification?

> `optional` **nationalIdentification**: [`NationalIdentification`](../../ivms/types/type-aliases/NationalIdentification.md)

#### Defined in

[types.ts:347](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L347)

***

### phone?

> `optional` **phone**: `string`

#### Defined in

[types.ts:349](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L349)

***

### type?

> `optional` **type**: [`PersonType`](../enumerations/PersonType.md)

#### Defined in

[types.ts:344](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L344)

***

### verified?

> `optional` **verified**: `boolean`

#### Defined in

[types.ts:345](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L345)

***

### website?

> `optional` **website**: `string`

#### Defined in

[types.ts:348](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L348)
