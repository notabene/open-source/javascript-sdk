[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / CallbackOptions

# Interface: CallbackOptions

Options for callback and redirect URIs

## Properties

### callback?

> `optional` **callback**: `string`

#### Defined in

[types.ts:977](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L977)

***

### redirectUri?

> `optional` **redirectUri**: `string`

#### Defined in

[types.ts:978](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L978)
