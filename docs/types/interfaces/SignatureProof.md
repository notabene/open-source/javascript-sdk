[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / SignatureProof

# Interface: SignatureProof

Interface for signature-based ownership proofs that use cryptographic message signing

## Remarks

Extends the base OwnershipProface to add signature-specific properties:
- Supports multiple signature standards like EIP-191, EIP-712, BIP-137, SIWE
- Includes the cryptographic proof signature string
- Contains an attestation message that was signed
- Records which wallet provider was used for signing

The signature proves ownership by demonstrating control of the private keys
associated with the claimed address.

## See

 - [ProofTypes](../enumerations/ProofTypes.md) For supported signature types
 - [OwnershipProof](OwnershipProof.md) For base proof properties

## Extends

- [`OwnershipProof`](OwnershipProof.md)

## Properties

### address

> **address**: \`$\{string\}:$\{string\}:$\{string\}\`

#### Inherited from

[`OwnershipProof`](OwnershipProof.md).[`address`](OwnershipProof.md#address)

#### Defined in

[types.ts:1065](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1065)

***

### attestation

> **attestation**: `string`

#### Defined in

[types.ts:1099](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1099)

***

### did

> **did**: \`did:$\{string\}:$\{string\}\`

#### Inherited from

[`OwnershipProof`](OwnershipProof.md).[`did`](OwnershipProof.md#did)

#### Defined in

[types.ts:1064](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1064)

***

### proof

> **proof**: `string`

#### Defined in

[types.ts:1098](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1098)

***

### status

> **status**: [`ProofStatus`](../enumerations/ProofStatus.md)

#### Inherited from

[`OwnershipProof`](OwnershipProof.md).[`status`](OwnershipProof.md#status)

#### Defined in

[types.ts:1063](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1063)

***

### type

> **type**: [`SIWE`](../enumerations/ProofTypes.md#siwe) \| [`SIWX`](../enumerations/ProofTypes.md#siwx) \| [`EIP191`](../enumerations/ProofTypes.md#eip191) \| [`EIP712`](../enumerations/ProofTypes.md#eip712) \| [`EIP1271`](../enumerations/ProofTypes.md#eip1271) \| [`BIP137`](../enumerations/ProofTypes.md#bip137) \| [`BIP322`](../enumerations/ProofTypes.md#bip322) \| [`BIP137_XPUB`](../enumerations/ProofTypes.md#bip137_xpub) \| [`TIP191`](../enumerations/ProofTypes.md#tip191) \| [`ED25519`](../enumerations/ProofTypes.md#ed25519)

#### Overrides

[`OwnershipProof`](OwnershipProof.md).[`type`](OwnershipProof.md#type)

#### Defined in

[types.ts:1086](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1086)

***

### wallet\_provider

> **wallet\_provider**: `string`

#### Defined in

[types.ts:1100](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1100)

***

### xpub?

> `optional` **xpub**: `string`

#### Defined in

[types.ts:1101](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L1101)
