[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / VASP

# Interface: VASP

A VASP agent acting on behalf of the counterparty

## Extends

- [`Agent`](Agent.md)

## Properties

### countryOfRegistration?

> `optional` **countryOfRegistration**: `string`

#### Defined in

[types.ts:302](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L302)

***

### did

> **did**: \`did:$\{string\}:$\{string\}\`

#### Inherited from

[`Agent`](Agent.md).[`did`](Agent.md#did)

#### Defined in

[types.ts:259](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L259)

***

### lei?

> `optional` **lei**: `string`

#### Defined in

[types.ts:299](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L299)

***

### logo?

> `optional` **logo**: `string`

#### Overrides

[`Agent`](Agent.md).[`logo`](Agent.md#logo)

#### Defined in

[types.ts:300](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L300)

***

### name?

> `optional` **name**: `string`

#### Inherited from

[`Agent`](Agent.md).[`name`](Agent.md#name)

#### Defined in

[types.ts:263](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L263)

***

### type

> **type**: [`AgentType`](../enumerations/AgentType.md)

#### Inherited from

[`Agent`](Agent.md).[`type`](Agent.md#type)

#### Defined in

[types.ts:260](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L260)

***

### url?

> `optional` **url**: `string`

#### Inherited from

[`Agent`](Agent.md).[`url`](Agent.md#url)

#### Defined in

[types.ts:262](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L262)

***

### verified?

> `optional` **verified**: `boolean`

#### Inherited from

[`Agent`](Agent.md).[`verified`](Agent.md#verified)

#### Defined in

[types.ts:264](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L264)

***

### website?

> `optional` **website**: `string`

#### Defined in

[types.ts:301](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L301)
