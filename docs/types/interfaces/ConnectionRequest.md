[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / ConnectionRequest

# Interface: ConnectionRequest

An object representing a connection request

## Extends

- [`ComponentRequest`](ComponentRequest.md)

## Properties

### asset

> **asset**: `string`

#### Defined in

[types.ts:575](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L575)

***

### customer?

> `optional` **customer**: [`Counterparty`](Counterparty.md)

#### Inherited from

[`ComponentRequest`](ComponentRequest.md).[`customer`](ComponentRequest.md#customer)

#### Defined in

[types.ts:496](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L496)

***

### requestId?

> `optional` **requestId**: `string`

#### Inherited from

[`ComponentRequest`](ComponentRequest.md).[`requestId`](ComponentRequest.md#requestid)

#### Defined in

[types.ts:495](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L495)
