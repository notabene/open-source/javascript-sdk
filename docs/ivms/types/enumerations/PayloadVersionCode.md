[**@notabene/javascript-sdk**](../../../README.md)

***

[@notabene/javascript-sdk](../../../modules.md) / [ivms/types](../README.md) / PayloadVersionCode

# Enumeration: PayloadVersionCode

The version of IVMS 101 to which the payload complies.

## Enumeration Members

### V101

> **V101**: `"101"`

Published May 2020

#### Defined in

[ivms/types.ts:365](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/ivms/types.ts#L365)

***

### V101\_2023

> **V101\_2023**: `"101.2023"`

Published August 2023

#### Defined in

[ivms/types.ts:367](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/ivms/types.ts#L367)
