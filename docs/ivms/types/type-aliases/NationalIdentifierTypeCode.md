[**@notabene/javascript-sdk**](../../../README.md)

***

[@notabene/javascript-sdk](../../../modules.md) / [ivms/types](../README.md) / NationalIdentifierTypeCode

# Type Alias: NationalIdentifierTypeCode

> **NationalIdentifierTypeCode**: `"ARNU"` \| `"CCPT"` \| `"RAID"` \| `"DRLC"` \| `"FIIN"` \| `"TXID"` \| `"SOCS"` \| `"IDCD"` \| `"LEIX"` \| `"MISC"`

National Identifier Type Code
Specifies the type of national identifier

## Defined in

[ivms/types.ts:54](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/ivms/types.ts#L54)
