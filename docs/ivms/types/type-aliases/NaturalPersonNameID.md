[**@notabene/javascript-sdk**](../../../README.md)

***

[@notabene/javascript-sdk](../../../modules.md) / [ivms/types](../README.md) / NaturalPersonNameID

# Type Alias: NaturalPersonNameID

> **NaturalPersonNameID**: `object`

Natural Person Name ID
Represents a name identifier for a natural person

## Type declaration

### nameIdentifierType?

> `optional` **nameIdentifierType**: [`NaturalPersonNameTypeCode`](NaturalPersonNameTypeCode.md)

Type of name identifier

### primaryIdentifier?

> `optional` **primaryIdentifier**: `string`

Primary identifier, maximum 100 characters

### secondaryIdentifier?

> `optional` **secondaryIdentifier**: `string`

Secondary identifier, maximum 100 characters

## Defined in

[ivms/types.ts:173](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/ivms/types.ts#L173)
