[**@notabene/javascript-sdk**](../../../README.md)

***

[@notabene/javascript-sdk](../../../modules.md) / [ivms/types](../README.md) / OriginatingVASP

# Type Alias: OriginatingVASP

> **OriginatingVASP**: `object`

Originating VASP
Represents the VASP which initiates the VA transfer

## Type declaration

### originatingVASP?

> `optional` **originatingVASP**: [`Person`](Person.md)

The originating VASP information

## Defined in

[ivms/types.ts:325](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/ivms/types.ts#L325)
