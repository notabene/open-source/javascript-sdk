[**@notabene/javascript-sdk**](../../../README.md)

***

[@notabene/javascript-sdk](../../../modules.md) / [ivms/types](../README.md) / IVMS101

# Type Alias: IVMS101

> **IVMS101**: `object`

IVMS101 definition

## Type declaration

### beneficiary?

> `optional` **beneficiary**: [`Beneficiary`](Beneficiary.md)

### beneficiaryVASP?

> `optional` **beneficiaryVASP**: [`BeneficiaryVASP`](BeneficiaryVASP.md)

### originatingVASP?

> `optional` **originatingVASP**: [`OriginatingVASP`](OriginatingVASP.md)

### originator?

> `optional` **originator**: [`Originator`](Originator.md)

### payloadMetadata?

> `optional` **payloadMetadata**: [`PayloadMetadata`](../interfaces/PayloadMetadata.md)

### transferPath?

> `optional` **transferPath**: [`TransferPath`](TransferPath.md)

## Defined in

[ivms/types.ts:375](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/ivms/types.ts#L375)
