[**@notabene/javascript-sdk**](../../../README.md)

***

[@notabene/javascript-sdk](../../../modules.md) / [ivms/types](../README.md) / BeneficiaryVASP

# Type Alias: BeneficiaryVASP

> **BeneficiaryVASP**: `object`

Beneficiary VASP
Represents the VASP which receives the VA transfer

## Type declaration

### beneficiaryVASP?

> `optional` **beneficiaryVASP**: [`Person`](Person.md)

The beneficiary VASP information

## Defined in

[ivms/types.ts:335](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/ivms/types.ts#L335)
