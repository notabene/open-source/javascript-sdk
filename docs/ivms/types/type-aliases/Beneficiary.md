[**@notabene/javascript-sdk**](../../../README.md)

***

[@notabene/javascript-sdk](../../../modules.md) / [ivms/types](../README.md) / Beneficiary

# Type Alias: Beneficiary

> **Beneficiary**: `object`

Beneficiary
Represents the receiver of the requested VA transfer

## Type declaration

### accountNumber?

> `optional` **accountNumber**: `string`[]

Array of account numbers, maximum 100 characters each

### beneficiaryPersons?

> `optional` **beneficiaryPersons**: [`Person`](Person.md)[]

Array of persons associated with the beneficiary

## Defined in

[ivms/types.ts:313](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/ivms/types.ts#L313)
