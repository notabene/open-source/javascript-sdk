[**@notabene/javascript-sdk**](../../../README.md)

***

[@notabene/javascript-sdk](../../../modules.md) / [ivms/types](../README.md) / LocalLegalPersonNameID

# Type Alias: LocalLegalPersonNameID

> **LocalLegalPersonNameID**: `object`

Local Legal Person Name ID
Represents a local name identifier for a legal person

## Type declaration

### legalPersonName?

> `optional` **legalPersonName**: `string`

Name of the legal person, maximum 100 characters in local format

### legalPersonNameIdentifierType?

> `optional` **legalPersonNameIdentifierType**: [`LegalPersonNameTypeCode`](LegalPersonNameTypeCode.md)

Type of legal person name identifier

## Defined in

[ivms/types.ts:221](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/ivms/types.ts#L221)
