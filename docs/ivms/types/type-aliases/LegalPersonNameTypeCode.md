[**@notabene/javascript-sdk**](../../../README.md)

***

[@notabene/javascript-sdk](../../../modules.md) / [ivms/types](../README.md) / LegalPersonNameTypeCode

# Type Alias: LegalPersonNameTypeCode

> **LegalPersonNameTypeCode**: `"LEGL"` \| `"SHRT"` \| `"TRAD"`

Legal Person Name Type Code
Specifies the type of name for a legal person

## Defined in

[ivms/types.ts:34](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/ivms/types.ts#L34)
