[**@notabene/javascript-sdk**](../../../README.md)

***

[@notabene/javascript-sdk](../../../modules.md) / [ivms/types](../README.md) / NaturalPersonNameTypeCode

# Type Alias: NaturalPersonNameTypeCode

> **NaturalPersonNameTypeCode**: `"ALIA"` \| `"BIRT"` \| `"MAID"` \| `"LEGL"` \| `"MISC"`

Natural Person Name Type Code
Specifies the type of name for a natural person

## Defined in

[ivms/types.ts:22](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/ivms/types.ts#L22)
