[**@notabene/javascript-sdk**](../../../README.md)

***

[@notabene/javascript-sdk](../../../modules.md) / [ivms/types](../README.md) / TransferPath

# Type Alias: TransferPath

> **TransferPath**: `object`

Transfer Path
Represents the path of intermediary VASPs in a transfer

## Type declaration

### transferPath?

> `optional` **transferPath**: [`IntermediaryVASP`](IntermediaryVASP.md)[]

Array of intermediary VASPs involved in the transfer

## Defined in

[ivms/types.ts:345](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/ivms/types.ts#L345)
