[**@notabene/javascript-sdk**](../../../README.md)

***

[@notabene/javascript-sdk](../../../modules.md) / [ivms/types](../README.md) / NaturalPersonName

# Type Alias: NaturalPersonName

> **NaturalPersonName**: `object`

Natural Person Name
Represents the full name structure for a natural person

## Type declaration

### localNameIdentifier?

> `optional` **localNameIdentifier**: [`LocalNaturalPersonNameID`](LocalNaturalPersonNameID.md)[]

Array of local name identifiers

### nameIdentifier?

> `optional` **nameIdentifier**: [`NaturalPersonNameID`](NaturalPersonNameID.md)[]

Array of name identifiers

### phoneticNameIdentifier?

> `optional` **phoneticNameIdentifier**: [`LocalNaturalPersonNameID`](LocalNaturalPersonNameID.md)[]

Array of phonetic name identifiers

## Defined in

[ivms/types.ts:187](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/ivms/types.ts#L187)
