[**@notabene/javascript-sdk**](../../../README.md)

***

[@notabene/javascript-sdk](../../../modules.md) / [ivms/types](../README.md) / DateAndPlaceOfBirth

# Type Alias: DateAndPlaceOfBirth

> **DateAndPlaceOfBirth**: `object`

Date and Place of Birth
Represents the date and place of birth for a natural person

## Type declaration

### dateOfBirth?

> `optional` **dateOfBirth**: [`ISODate`](ISODate.md)

Date of birth in ISO 8601 format (YYYY-MM-DD)

### placeOfBirth?

> `optional` **placeOfBirth**: `string`

Place of birth (max 70 characters)

## Defined in

[ivms/types.ts:107](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/ivms/types.ts#L107)
