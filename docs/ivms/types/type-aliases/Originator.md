[**@notabene/javascript-sdk**](../../../README.md)

***

[@notabene/javascript-sdk](../../../modules.md) / [ivms/types](../README.md) / Originator

# Type Alias: Originator

> **Originator**: `object`

Originator
Represents the account holder who initiates the VA transfer

## Type declaration

### accountNumber?

> `optional` **accountNumber**: `string`[]

Array of account numbers, maximum 100 characters each

### originatorPersons?

> `optional` **originatorPersons**: [`Person`](Person.md)[]

Array of persons associated with the originator

## Defined in

[ivms/types.ts:301](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/ivms/types.ts#L301)
