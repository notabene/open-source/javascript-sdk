[**@notabene/javascript-sdk**](../../../README.md)

***

[@notabene/javascript-sdk](../../../modules.md) / [ivms/types](../README.md) / AddressTypeCode

# Type Alias: AddressTypeCode

> **AddressTypeCode**: `"HOME"` \| `"BIZZ"` \| `"GEOG"`

Address Type Code
Specifies the type of address

## Defined in

[ivms/types.ts:44](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/ivms/types.ts#L44)
