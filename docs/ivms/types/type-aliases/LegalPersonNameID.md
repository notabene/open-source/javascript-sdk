[**@notabene/javascript-sdk**](../../../README.md)

***

[@notabene/javascript-sdk](../../../modules.md) / [ivms/types](../README.md) / LegalPersonNameID

# Type Alias: LegalPersonNameID

> **LegalPersonNameID**: `object`

Legal Person Name ID
Represents a name identifier for a legal person

## Type declaration

### legalPersonName

> **legalPersonName**: `string`

Name by which the legal person is known

### legalPersonNameIdentifierType

> **legalPersonNameIdentifierType**: [`LegalPersonNameTypeCode`](LegalPersonNameTypeCode.md)

The nature of the name specified

## Defined in

[ivms/types.ts:233](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/ivms/types.ts#L233)
