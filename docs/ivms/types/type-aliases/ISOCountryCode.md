[**@notabene/javascript-sdk**](../../../README.md)

***

[@notabene/javascript-sdk](../../../modules.md) / [ivms/types](../README.md) / ISOCountryCode

# Type Alias: ISOCountryCode

> **ISOCountryCode**: `string`

ISO-3166 Alpha-2 country code

## Example

```ts
"US" for United States, "GB" for United Kingdom
@public
```

## Defined in

[ivms/types.ts:6](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/ivms/types.ts#L6)
