[**@notabene/javascript-sdk**](../../../README.md)

***

[@notabene/javascript-sdk](../../../modules.md) / [ivms/types](../README.md) / TransliterationMethodCode

# Type Alias: TransliterationMethodCode

> **TransliterationMethodCode**: `"arab"` \| `"aran"` \| `"armn"` \| `"cyrl"` \| `"deva"` \| `"geor"` \| `"grek"` \| `"hani"` \| `"hebr"` \| `"kana"` \| `"kore"` \| `"thai"` \| `"othr"`

Transliteration Method Code
Specifies the method used to transliterate text

## Defined in

[ivms/types.ts:71](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/ivms/types.ts#L71)
