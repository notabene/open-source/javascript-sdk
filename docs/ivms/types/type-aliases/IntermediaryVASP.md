[**@notabene/javascript-sdk**](../../../README.md)

***

[@notabene/javascript-sdk](../../../modules.md) / [ivms/types](../README.md) / IntermediaryVASP

# Type Alias: IntermediaryVASP

> **IntermediaryVASP**: `object`

Intermediary VASP
Represents an intermediary Virtual Asset Service Provider

## Type declaration

### intermediaryVASP?

> `optional` **intermediaryVASP**: [`Person`](Person.md)

The intermediary VASP information

### sequence?

> `optional` **sequence**: `number`

The sequence number of this VASP in the transfer path

## Defined in

[ivms/types.ts:289](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/ivms/types.ts#L289)
