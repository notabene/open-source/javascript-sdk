[**@notabene/javascript-sdk**](../../../README.md)

***

[@notabene/javascript-sdk](../../../modules.md) / [ivms/types](../README.md) / ISODate

# Type Alias: ISODate

> **ISODate**: \`$\{number\}-$\{number\}-$\{number\}\`

A point in time, represented as a day within the calendar year. Compliant with ISO 8601.
Format: YYYY-MM-DD

## Example

```ts
"2023-05-15" for May 15, 2023
@public
```

## Defined in

[ivms/types.ts:14](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/ivms/types.ts#L14)
