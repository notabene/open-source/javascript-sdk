[**@notabene/javascript-sdk**](../../../README.md)

***

[@notabene/javascript-sdk](../../../modules.md) / [ivms/types](../README.md) / LegalPersonName

# Type Alias: LegalPersonName

> **LegalPersonName**: `object`

Legal Person Name
Represents the full name structure for a legal person

## Type declaration

### localNameIdentifier?

> `optional` **localNameIdentifier**: [`LocalLegalPersonNameID`](LocalLegalPersonNameID.md)[]

Array of local name identifiers

### nameIdentifier

> **nameIdentifier**: [`LegalPersonNameID`](LegalPersonNameID.md)[]

Array of name identifiers

### phoneticNameIdentifier?

> `optional` **phoneticNameIdentifier**: [`LocalLegalPersonNameID`](LocalLegalPersonNameID.md)[]

Array of phonetic name identifiers

## Defined in

[ivms/types.ts:245](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/ivms/types.ts#L245)
