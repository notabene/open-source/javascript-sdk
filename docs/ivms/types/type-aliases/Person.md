[**@notabene/javascript-sdk**](../../../README.md)

***

[@notabene/javascript-sdk](../../../modules.md) / [ivms/types](../README.md) / Person

# Type Alias: Person

> **Person**: `object`

Person
Represents either a natural person or a legal person

## Type declaration

### legalPerson?

> `optional` **legalPerson**: [`LegalPerson`](LegalPerson.md)

Legal person information

### naturalPerson?

> `optional` **naturalPerson**: [`NaturalPerson`](NaturalPerson.md)

Natural person information

## Defined in

[ivms/types.ts:277](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/ivms/types.ts#L277)
