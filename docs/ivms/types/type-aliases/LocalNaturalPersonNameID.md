[**@notabene/javascript-sdk**](../../../README.md)

***

[@notabene/javascript-sdk](../../../modules.md) / [ivms/types](../README.md) / LocalNaturalPersonNameID

# Type Alias: LocalNaturalPersonNameID

> **LocalNaturalPersonNameID**: `object`

Local Natural Person Name ID
Represents a local name identifier for a natural person

## Type declaration

### nameIdentifierType?

> `optional` **nameIdentifierType**: [`NaturalPersonNameTypeCode`](NaturalPersonNameTypeCode.md)

Type of name identifier

### primaryIdentifier?

> `optional` **primaryIdentifier**: `string`

Primary identifier, maximum 100 characters in local format

### secondaryIdentifier?

> `optional` **secondaryIdentifier**: `string`

Secondary identifier, maximum 100 characters in local format

## Defined in

[ivms/types.ts:159](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/ivms/types.ts#L159)
