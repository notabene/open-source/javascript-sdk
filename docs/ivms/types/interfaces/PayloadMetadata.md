[**@notabene/javascript-sdk**](../../../README.md)

***

[@notabene/javascript-sdk](../../../modules.md) / [ivms/types](../README.md) / PayloadMetadata

# Interface: PayloadMetadata

6.7
Data describing the contents of the payload.

## Properties

### payloadVersion

> **payloadVersion**: [`PayloadVersionCode`](../enumerations/PayloadVersionCode.md)

The version of IVMS 101 to which the payload complies.

#### Defined in

[ivms/types.ts:358](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/ivms/types.ts#L358)

***

### transliterationMethod?

> `optional` **transliterationMethod**: [`TransliterationMethodCode`](../type-aliases/TransliterationMethodCode.md)[]

The method used to map from a national system of writing to Latin script.

#### Defined in

[ivms/types.ts:356](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/ivms/types.ts#L356)
