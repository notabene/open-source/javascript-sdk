[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / ivms/types

# ivms/types

## Index

### Enumerations

- [PayloadVersionCode](enumerations/PayloadVersionCode.md)

### Interfaces

- [PayloadMetadata](interfaces/PayloadMetadata.md)

### Type Aliases

- [Address](type-aliases/Address.md)
- [AddressTypeCode](type-aliases/AddressTypeCode.md)
- [Beneficiary](type-aliases/Beneficiary.md)
- [BeneficiaryVASP](type-aliases/BeneficiaryVASP.md)
- [DateAndPlaceOfBirth](type-aliases/DateAndPlaceOfBirth.md)
- [IntermediaryVASP](type-aliases/IntermediaryVASP.md)
- [ISOCountryCode](type-aliases/ISOCountryCode.md)
- [ISODate](type-aliases/ISODate.md)
- [IVMS101](type-aliases/IVMS101.md)
- [LegalPerson](type-aliases/LegalPerson.md)
- [LegalPersonName](type-aliases/LegalPersonName.md)
- [LegalPersonNameID](type-aliases/LegalPersonNameID.md)
- [LegalPersonNameTypeCode](type-aliases/LegalPersonNameTypeCode.md)
- [LocalLegalPersonNameID](type-aliases/LocalLegalPersonNameID.md)
- [LocalNaturalPersonNameID](type-aliases/LocalNaturalPersonNameID.md)
- [NationalIdentification](type-aliases/NationalIdentification.md)
- [NationalIdentifierTypeCode](type-aliases/NationalIdentifierTypeCode.md)
- [NaturalPerson](type-aliases/NaturalPerson.md)
- [NaturalPersonName](type-aliases/NaturalPersonName.md)
- [NaturalPersonNameID](type-aliases/NaturalPersonNameID.md)
- [NaturalPersonNameTypeCode](type-aliases/NaturalPersonNameTypeCode.md)
- [OriginatingVASP](type-aliases/OriginatingVASP.md)
- [Originator](type-aliases/Originator.md)
- [Person](type-aliases/Person.md)
- [TransferPath](type-aliases/TransferPath.md)
- [TransliterationMethodCode](type-aliases/TransliterationMethodCode.md)
