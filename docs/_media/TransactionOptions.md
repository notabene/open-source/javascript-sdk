[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [types](../README.md) / TransactionOptions

# Interface: TransactionOptions

Configuration options for Transaction components

## Properties

### allowedAgentTypes?

> `optional` **allowedAgentTypes**: [`AgentType`](../enumerations/AgentType.md)[]

#### Defined in

[types.ts:782](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L782)

***

### allowedCounterpartyTypes?

> `optional` **allowedCounterpartyTypes**: [`PersonType`](../enumerations/PersonType.md)[]

#### Defined in

[types.ts:783](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L783)

***

### fields?

> `optional` **fields**: [`FieldTypes`](../type-aliases/FieldTypes.md)

#### Defined in

[types.ts:784](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L784)

***

### hide?

> `optional` **hide**: [`ValidationSections`](../enumerations/ValidationSections.md)[]

#### Defined in

[types.ts:786](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L786)

***

### proofs?

> `optional` **proofs**: `object`

#### deminimis?

> `optional` **deminimis**: [`ThresholdOptions`](ThresholdOptions.md)

#### fallbacks?

> `optional` **fallbacks**: [`ProofTypes`](../enumerations/ProofTypes.md)[]

#### microTransfer?

> `optional` **microTransfer**: `object`

##### microTransfer.amountSubunits

> **amountSubunits**: `string`

##### microTransfer.destination

> **destination**: `string`

##### microTransfer.timeout?

> `optional` **timeout**: `number`

#### reuseProof?

> `optional` **reuseProof**: `boolean`

#### Defined in

[types.ts:772](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L772)

***

### vasps?

> `optional` **vasps**: [`VASPOptions`](../type-aliases/VASPOptions.md)

#### Defined in

[types.ts:785](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/types.ts#L785)
