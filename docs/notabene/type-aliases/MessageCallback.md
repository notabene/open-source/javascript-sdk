[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [notabene](../README.md) / MessageCallback

# Type Alias: MessageCallback()\<T\>

> **MessageCallback**\<`T`\>: (`message`) => `void`

Callback function for handling component messages.

## Type Parameters

• **T**

The type of data contained in the component message

## Parameters

### message

[`ComponentMessage`](../../types/type-aliases/ComponentMessage.md)\<`T`\>

The message object containing the component data and type

## Returns

`void`

## Defined in

[utils/MessageEventManager.ts:11](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/utils/MessageEventManager.ts#L11)
