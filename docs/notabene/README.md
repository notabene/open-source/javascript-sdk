[**@notabene/javascript-sdk**](../README.md)

***

[@notabene/javascript-sdk](../modules.md) / notabene

# notabene

## Index

### Classes

- [default](classes/default.md)
- [EmbeddedComponent](classes/EmbeddedComponent.md)

### Interfaces

- [NotabeneConfig](interfaces/NotabeneConfig.md)

### Type Aliases

- [MessageCallback](type-aliases/MessageCallback.md)

### Functions

- [decodeFragmentToObject](functions/decodeFragmentToObject.md)

## References

### Agent

Re-exports [Agent](../types/interfaces/Agent.md)

***

### AgentType

Re-exports [AgentType](../types/enumerations/AgentType.md)

***

### BlockchainAddress

Re-exports [BlockchainAddress](../types/type-aliases/BlockchainAddress.md)

***

### CAIP10

Re-exports [CAIP10](../types/type-aliases/CAIP10.md)

***

### CAIP19

Re-exports [CAIP19](../types/type-aliases/CAIP19.md)

***

### CAIP2

Re-exports [CAIP2](../types/type-aliases/CAIP2.md)

***

### CallbackOptions

Re-exports [CallbackOptions](../types/interfaces/CallbackOptions.md)

***

### Cancel

Re-exports [Cancel](../types/type-aliases/Cancel.md)

***

### CMType

Re-exports [CMType](../types/enumerations/CMType.md)

***

### Completed

Re-exports [Completed](../types/type-aliases/Completed.md)

***

### ComponentMessage

Re-exports [ComponentMessage](../types/type-aliases/ComponentMessage.md)

***

### ComponentResponse

Re-exports [ComponentResponse](../types/interfaces/ComponentResponse.md)

***

### ConnectionOptions

Re-exports [ConnectionOptions](../types/type-aliases/ConnectionOptions.md)

***

### ConnectionRequest

Re-exports [ConnectionRequest](../types/interfaces/ConnectionRequest.md)

***

### Counterparty

Re-exports [Counterparty](../types/interfaces/Counterparty.md)

***

### CryptoCredential

Re-exports [CryptoCredential](../types/type-aliases/CryptoCredential.md)

***

### DeclarationProof

Re-exports [DeclarationProof](../types/interfaces/DeclarationProof.md)

***

### Deposit

Re-exports [Deposit](../types/interfaces/Deposit.md)

***

### DepositRequest

Re-exports [DepositRequest](../types/interfaces/DepositRequest.md)

***

### DepositRequestOptions

Re-exports [DepositRequestOptions](../types/interfaces/DepositRequestOptions.md)

***

### Destination

Re-exports [Destination](../types/type-aliases/Destination.md)

***

### DID

Re-exports [DID](../types/type-aliases/DID.md)

***

### DTI

Re-exports [DTI](../types/type-aliases/DTI.md)

***

### Error

Re-exports [Error](../types/type-aliases/Error.md)

***

### ErrorIdentifierCode

Re-exports [ErrorIdentifierCode](../types/enumerations/ErrorIdentifierCode.md)

***

### FieldTypes

Re-exports [FieldTypes](../types/type-aliases/FieldTypes.md)

***

### HMType

Re-exports [HMType](../types/enumerations/HMType.md)

***

### HostMessage

Re-exports [HostMessage](../types/type-aliases/HostMessage.md)

***

### InvalidValue

Re-exports [InvalidValue](../types/type-aliases/InvalidValue.md)

***

### IVMS101

Re-exports [IVMS101](../ivms/types/type-aliases/IVMS101.md)

***

### LegalPerson

Re-exports [LegalPerson](../types/interfaces/LegalPerson.md)

***

### LEI

Re-exports [LEI](../types/type-aliases/LEI.md)

***

### MicroTransferProof

Re-exports [MicroTransferProof](../types/interfaces/MicroTransferProof.md)

***

### NationalIdentification

Re-exports [NationalIdentification](../ivms/types/type-aliases/NationalIdentification.md)

***

### NaturalPerson

Re-exports [NaturalPerson](../types/interfaces/NaturalPerson.md)

***

### NotabeneAsset

Re-exports [NotabeneAsset](../types/type-aliases/NotabeneAsset.md)

***

### OwnershipProof

Re-exports [OwnershipProof](../types/interfaces/OwnershipProof.md)

***

### PersonType

Re-exports [PersonType](../types/enumerations/PersonType.md)

***

### ProofStatus

Re-exports [ProofStatus](../types/enumerations/ProofStatus.md)

***

### ProofTypes

Re-exports [ProofTypes](../types/enumerations/ProofTypes.md)

***

### Ready

Re-exports [Ready](../types/type-aliases/Ready.md)

***

### ResizeRequest

Re-exports [ResizeRequest](../types/type-aliases/ResizeRequest.md)

***

### ScreenshotProof

Re-exports [ScreenshotProof](../types/interfaces/ScreenshotProof.md)

***

### SignatureProof

Re-exports [SignatureProof](../types/interfaces/SignatureProof.md)

***

### Status

Re-exports [Status](../types/enumerations/Status.md)

***

### Theme

Re-exports [Theme](../types/type-aliases/Theme.md)

***

### ThresholdOptions

Re-exports [ThresholdOptions](../types/interfaces/ThresholdOptions.md)

***

### Transaction

Re-exports [Transaction](../types/interfaces/Transaction.md)

***

### TransactionAsset

Re-exports [TransactionAsset](../types/type-aliases/TransactionAsset.md)

***

### TransactionOptions

Re-exports [TransactionOptions](../types/interfaces/TransactionOptions.md)

***

### TransactionResponse

Re-exports [TransactionResponse](../types/interfaces/TransactionResponse.md)

***

### TravelAddress

Re-exports [TravelAddress](../types/type-aliases/TravelAddress.md)

***

### UpdateValue

Re-exports [UpdateValue](../types/type-aliases/UpdateValue.md)

***

### V1Transaction

Re-exports [V1Transaction](../types/type-aliases/V1Transaction.md)

***

### ValidationError

Re-exports [ValidationError](../types/type-aliases/ValidationError.md)

***

### ValidationSections

Re-exports [ValidationSections](../types/enumerations/ValidationSections.md)

***

### VASP

Re-exports [VASP](../types/interfaces/VASP.md)

***

### VASPOptions

Re-exports [VASPOptions](../types/type-aliases/VASPOptions.md)

***

### VASPSearchControl

Re-exports [VASPSearchControl](../types/enumerations/VASPSearchControl.md)

***

### Wallet

Re-exports [Wallet](../types/interfaces/Wallet.md)

***

### Withdrawal

Re-exports [Withdrawal](../types/interfaces/Withdrawal.md)
