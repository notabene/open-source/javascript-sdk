[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [notabene](../README.md) / decodeFragmentToObject

# Function: decodeFragmentToObject()

> **decodeFragmentToObject**(`fragment`): `Record`\<`string`, `string`\>

Decodes a URL fragment into an object

## Parameters

### fragment

`string`

The URL fragment to decode

## Returns

`Record`\<`string`, `string`\>

An object containing the decoded key-value pairs

## Defined in

[utils/urls.ts:22](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/utils/urls.ts#L22)
