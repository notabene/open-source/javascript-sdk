[**@notabene/javascript-sdk**](../../README.md)

***

[@notabene/javascript-sdk](../../modules.md) / [notabene](../README.md) / NotabeneConfig

# Interface: NotabeneConfig

Configuration for the Notabene SDK

## Properties

### authToken?

> `optional` **authToken**: `string`

The authentication token for the Notabene API

#### Defined in

[notabene.ts:153](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/notabene.ts#L153)

***

### locale?

> `optional` **locale**: `string`

The locale to use for the UX components

#### Defined in

[notabene.ts:168](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/notabene.ts#L168)

***

### nodeUrl?

> `optional` **nodeUrl**: `string`

The URL of the Notabene API node

#### Defined in

[notabene.ts:148](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/notabene.ts#L148)

***

### theme?

> `optional` **theme**: [`Theme`](../../types/type-aliases/Theme.md)

Custom theme configuration for the UX components

#### Defined in

[notabene.ts:163](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/notabene.ts#L163)

***

### uxUrl?

> `optional` **uxUrl**: `string`

The URL of the Notabene UX components

#### Defined in

[notabene.ts:158](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/7c1287c0f294e150fbd0ad5bc728bba0920f5ad7/src/notabene.ts#L158)
