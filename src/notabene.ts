import EmbeddedComponent from './components/EmbeddedComponent';
import type {
  Agent,
  BlockchainAddress,
  CAIP10,
  CAIP19,
  CAIP2,
  CallbackOptions,
  Cancel,
  Completed,
  ComponentMessage,
  ComponentResponse,
  ConnectionOptions,
  ConnectionRequest,
  Counterparty,
  CryptoCredential,
  DeclarationProof,
  Deposit,
  DepositRequest,
  DepositRequestOptions,
  Destination,
  DID,
  DTI,
  Error,
  FieldTypes,
  HostMessage,
  InvalidValue,
  IVMS101,
  LegalPerson,
  LEI,
  MicroTransferProof,
  NationalIdentification,
  NaturalPerson,
  NotabeneAsset,
  OwnershipProof,
  Ready,
  ResizeRequest,
  ScreenshotProof,
  SignatureProof,
  Theme,
  ThresholdOptions,
  Transaction,
  TransactionAsset,
  TransactionOptions,
  TransactionResponse,
  TravelAddress,
  UpdateValue,
  V1Transaction,
  ValidationError,
  VASP,
  VASPOptions,
  Wallet,
  Withdrawal,
} from './types';
import {
  AgentType,
  CMType,
  ErrorIdentifierCode,
  HMType,
  PersonType,
  ProofStatus,
  ProofTypes,
  Status,
  ValidationSections,
  VASPSearchControl,
} from './types';
import { type MessageCallback } from './utils/MessageEventManager';
import { decodeFragmentToObject, encodeObjectToFragment } from './utils/urls';
// Must be exported for React Native SDK to use
export { default as EmbeddedComponent } from './components/EmbeddedComponent';
export {
  AgentType,
  CMType,
  decodeFragmentToObject,
  ErrorIdentifierCode,
  HMType,
  PersonType,
  ProofStatus,
  ProofTypes,
  Status,
  ValidationSections,
  VASPSearchControl,
};
export type {
  Agent,
  BlockchainAddress,
  CAIP10,
  CAIP19,
  CAIP2,
  CallbackOptions,
  Cancel,
  Completed,
  ComponentMessage,
  ComponentResponse,
  ConnectionOptions,
  ConnectionRequest,
  Counterparty,
  CryptoCredential,
  DeclarationProof,
  Deposit,
  DepositRequest,
  DepositRequestOptions,
  Destination,
  DID,
  DTI,
  Error,
  FieldTypes,
  HostMessage,
  InvalidValue,
  IVMS101,
  LegalPerson,
  LEI,
  MessageCallback,
  MicroTransferProof,
  NationalIdentification,
  NaturalPerson,
  NotabeneAsset,
  OwnershipProof,
  Ready,
  ResizeRequest,
  ScreenshotProof,
  SignatureProof,
  Theme,
  ThresholdOptions,
  Transaction,
  TransactionAsset,
  TransactionOptions,
  TransactionResponse,
  TravelAddress,
  UpdateValue,
  V1Transaction,
  ValidationError,
  VASP,
  VASPOptions,
  Wallet,
  Withdrawal,
};

/**
 * Configuration for the Notabene SDK
 *
 * @public
 */
export interface NotabeneConfig {
  /**
   * The URL of the Notabene API node
   */
  nodeUrl?: string;

  /**
   * The authentication token for the Notabene API
   */
  authToken?: string;

  /**
   * The URL of the Notabene UX components
   */
  uxUrl?: string;

  /**
   * Custom theme configuration for the UX components
   */
  theme?: Theme;

  /**
   * The locale to use for the UX components
   */
  locale?: string;
}
/**
 * Primary constructor for Notabene UX elements
 *
 * This class provides methods to create and manage various Notabene components
 * such as withdrawal assist, deposit assist, connect, and deposit request.
 * It also handles URL generation and fragment decoding for these components.
 *
 * @public
 */
export default class Notabene {
  private nodeUrl?: string;
  private authToken?: string;
  private uxUrl: string;
  private theme?: Theme;
  private locale?: string;

  /**
   * Creates a new instance of the Notabene SDK
   *
   * @param config - Configuration options for the Notabene SDK
   */
  constructor(config: NotabeneConfig) {
    this.uxUrl = config.uxUrl || 'https://connect.notabene.id';
    this.nodeUrl = config.nodeUrl;
    this.authToken = config.authToken;
    this.theme = config.theme;
    this.locale = config.locale;
  }

  /**
   * Generates a URL for a Notabene component
   *
   * @param path - The path of the component
   * @param value - Transaction data
   * @param configuration - Optional transaction configuration
   * @param callbacks - Optional callback configuration
   * @returns component URL
   * @internal
   */
  componentUrl<V, O>(
    path: string,
    value: V,
    configuration?: O,
    callbacks?: CallbackOptions,
  ): string {
    const url = new URL(this.uxUrl);
    url.pathname = path;

    const hash = encodeObjectToFragment({
      authToken: this.authToken,
      value,
      configuration,
    });
    url.hash = hash;
    if (this.nodeUrl) url.searchParams.set('nodeUrl', this.nodeUrl);

    if (this.theme) {
      url.searchParams.set('theme', JSON.stringify(this.theme));
    }
    if (this.locale) {
      url.searchParams.set('locale', this.locale);
    }
    if (callbacks) {
      if (callbacks.callback)
        url.searchParams.set('callback_url', callbacks.callback);
      if (callbacks.redirectUri)
        url.searchParams.set('redirect_uri', callbacks.redirectUri);
    }
    return url.toString();
  }

  /**
   * Creates a new embedded component
   *
   * @param path - The path of the component
   * @param value - Transaction data
   * @param options - Optional transaction options
   * @param callbacks - Optional callback configuration
   * @returns A new EmbeddedComponent instance
   * @internal
   */
  createComponent<V, O>(
    path: string,
    value: Partial<V>,
    options?: O,
    callbacks?: CallbackOptions,
  ) {
    return new EmbeddedComponent<V, O>(
      this.componentUrl(path, value, options, callbacks),
      value,
      options,
    );
  }

  /**
   * Creates a withdrawal assist component
   *
   * @param value - Withdrawal transaction data
   * @param options - Optional transaction options
   * @param callbacks - Optional callback configuration
   * @returns A new EmbeddedComponent instance for withdrawal assistance
   */
  public createWithdrawalAssist(
    value: Partial<Withdrawal>,
    options?: TransactionOptions,
    callbacks?: CallbackOptions,
  ) {
    return this.createComponent<Withdrawal, TransactionOptions>(
      'withdrawal-assist',
      value,
      options,
      callbacks,
    );
  }

  /**
   * Creates a connect component
   *
   * @param value - Connection request data
   * @param options - Optional transaction options
   * @param callbacks - Optional callback configuration
   * @returns A new EmbeddedComponent instance for connection
   * @alpha
   */
  public createConnectWallet(
    value: ConnectionRequest,
    options?: ConnectionOptions,
    callbacks?: CallbackOptions,
  ) {
    return this.createComponent<ConnectionRequest, ConnectionOptions>(
      'connect',
      value,
      options,
      callbacks,
    );
  }

  /**
   * Creates a deposit request component
   *
   * @param value - Deposit request data
   * @param options - Optional transaction options
   * @param callbacks - Optional callback configuration
   * @returns A new EmbeddedComponent instance for deposit requests
   * @public
   */
  public createDepositRequest(
    value: DepositRequest,
    options?: DepositRequestOptions,
    callbacks?: CallbackOptions,
  ) {
    return this.createComponent<DepositRequest, DepositRequestOptions>(
      'deposit-request',
      value,
      options,
      callbacks,
    );
  }

  /**
   * Creates a deposit assist component
   *
   * @param value - Partial deposit transaction data
   * @param options - Optional transaction options
   * @param callbacks - Optional callback configuration
   * @returns A new EmbeddedComponent instance for deposit assistance
   * @public
   */
  public createDepositAssist(
    value: Partial<Deposit>,
    options?: TransactionOptions,
    callbacks?: CallbackOptions,
  ) {
    return this.createComponent<Deposit, TransactionOptions>(
      'deposit-assist',
      value,
      options,
      callbacks,
    );
  }
}
