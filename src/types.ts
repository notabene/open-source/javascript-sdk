import type {
  Address,
  Beneficiary,
  ISOCountryCode,
  ISODate,
  IVMS101,
  NationalIdentification,
  Originator,
} from './ivms/types';

export type {
  BeneficiaryVASP,
  OriginatingVASP,
  PayloadMetadata,
  TransferPath,
} from './ivms/types';
export type {
  Address,
  Beneficiary,
  ISOCountryCode,
  ISODate,
  NationalIdentification,
  Originator,
};
/**
 * Interoperable Virtual Asset Service Provider (VASP) Messaging Standard
 * @public
 */
export type { IVMS101 };

/**
 * UUID v4 string identifier
 * A universally unique identifier that follows RFC 4122 format
 * Format: 8-4-4-4-12 hexadecimal digits
 * @example "550e8400-e29b-41d4-a716-446655440000"
 * @see {@link https://tools.ietf.org/html/rfc4122 | RFC4122}
 * @public
 */
export type UUID = string;

/**
 * Chain Agnostic Blockchain Identifier (CAIP-2)
 * Represents a blockchain in a chain-agnostic way following the CAIP-2 specification.
 * The identifier consists of a namespace and reference separated by a colon.
 *
 * Format: `namespace:reference`
 * - namespace: Represents the blockchain namespace (e.g. 'eip155', 'bip122', 'cosmos')
 * - reference: Chain-specific identifier within that namespace
 *
 * @example "eip155:1" // Ethereum Mainnet
 * @example "bip122:000000000019d6689c085ae165831e934ff763ae46a2a6c172b3f1b60a8ce26f" // Bitcoin Mainnet
 * @example "cosmos:cosmoshub-3" // Cosmos Hub Mainnet
 * @see {@link https://github.com/ChainAgnostic/CAIPs/blob/master/CAIPs/caip-2.md | CAIP-2 Specification}
 * @public
 */
export type CAIP2 = `${string}:${string}`;

/**
 * Chain Agnostic Account Identifier (CAIP-10)
 * Represents an account/address on a specific blockchain following the CAIP-10 specification.
 * Extends CAIP-2 by adding the account address specific to that chain.
 *
 * Format: `{caip2}:{address}`
 * - caip2: The CAIP-2 chain identifier (e.g. 'eip155:1')
 * - address: Chain-specific account address format
 *
 * @example "eip155:1:0x742d35Cc6634C0532925a3b844Bc454e4438f44e" // Ethereum account on mainnet
 * @example "bip122:000000000019d6689c085ae165831e934ff763ae46a2a6c172b3f1b60a8ce26f:128Lkh3S7CkDTBZ8W7BbpsN3YYizJMp8p6" // Bitcoin account on mainnet
 * @example "cosmos:cosmoshub-3:cosmos1t2uflqwqe0fsj0shcfkrvpukewcw40yjj6hdc0" // Cosmos account
 * @see {@link https://github.com/ChainAgnostic/CAIPs/blob/master/CAIPs/caip-10.md | CAIP-10 Specification}
 * @public
 */
export type CAIP10 = `${CAIP2}:${string}`;

/**
 * Chain Agnostic Asset Identifier (CAIP-19)
 * Represents an asset/token on a specific blockchain following the CAIP-19 specification.
 * Extends CAIP-2 by adding asset type and identifier information.
 *
 * Format: `{caip2}/{asset_namespace}:{asset_reference}`
 * - caip2: The CAIP-2 chain identifier (e.g. 'eip155:1')
 * - asset_namespace: The asset standard (e.g. 'erc20', 'erc721', 'slip44')
 * - asset_reference: Chain/standard-specific asset identifier
 *
 * @example "eip155:1/erc20:0x6b175474e89094c44da98b954eedeac495271d0f" // DAI token on Ethereum mainnet
 * @example "eip155:1/erc721:0x06012c8cf97BEaD5deAe237070F9587f8E7A266d" // CryptoKitties NFT contract
 * @example "cosmos:cosmoshub-3/slip44:118" // ATOM token on Cosmos Hub
 * @see {@link https://github.com/ChainAgnostic/CAIPs/blob/master/CAIPs/caip-19.md | CAIP-19 Specification}
 * @public
 */
export type CAIP19 = `${CAIP2}/${string}:${string}`;

/**
 * Chain Agnostic Payload Identifier
 * @public
 */
export type CAIP220 = string;

/**
 * Digital Token Identifier (DTI) following ISO 24165 standard
 *
 * @remarks
 * A standardized identifier for digital assets and cryptocurrencies. The DTI system
 * provides unique and unambiguous identification of digital tokens, supporting interoperability
 * and clarity in financial markets.
 *
 * Format: `DTI[NNNNN]` where N is a digit
 *
 * @example "DTI00001" // Example DTI for Bitcoin
 * @example "DTI00002" // Example DTI for Ethereum
 *
 * @see {@link https://dtif.org/ | Digital Token Identifier Foundation}
 * @see {@link https://www.iso.org/standard/77895.html | ISO 24165}
 * @public
 */

export type DTI = string;

/**
 * Notabene Asset Identifier
 * @public
 */

/**
 * Internal identifier for assets in the Notabene system
 *
 * @remarks
 * A standardized string format used within Notabene to identify cryptocurrencies,
 * tokens, and other digital assets. This is Notabene's legacy asset identification
 * system that may be used alongside CAIP-19 and DTI identifiers.
 *
 * @example "ETH_USDT" // USDT token on Ethereum
 * @example "BTC" // Bitcoin
 * @see {@link CAIP19} For chain-agnostic asset identifiers
 * @see {@link DTI} For ISO standardized identifiers
 * @public
 */

export type NotabeneAsset = string;

/**
 * The asset of a transaction either a Notabene asset, a CAIP-19 asset, or a DTI.
 * @public
 */
export type TransactionAsset = NotabeneAsset | CAIP19 | DTI;

/**
 * A blockchain address
 * @public
 */

/**
 * A native blockchain address string
 *
 * @remarks
 * Represents a blockchain address in the native format specific to a particular chain.
 * This could be an Ethereum address, Bitcoin address, or other chain-specific format.
 * The address format and validation rules depend on the underlying blockchain.
 *
 * @example "0x742d35Cc6634C0532925a3b844Bc454e4438f44e" // Ethereum address
 * @example "1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa" // Bitcoin address
 * @example "cosmos1t2uflqwqe0fsj0shcfkrvpukewcw40yjj6hdc0" // Cosmos address
 * @public
 */

export type BlockchainAddress = string;

/**
 * A travel address
 * @public

 * A standardized travel rule address format
 *
 * @remarks
 * Represents a special address format used for travel rule compliance. Travel addresses
 * are prefixed with 'ta' and contain encoded information about the transaction
 * and counterparty details required for travel rule reporting.
 *
 * The format ensures consistent handling of travel rule data across different
 * VASPs and blockchain networks while maintaining privacy.
 *
 * @example "ta1234abcd..." // Example travel rule address
 * @see {@link BlockchainAddress} For native chain addresses
 * @see {@link CAIP10} For chain-agnostic addresses

 */ export type TravelAddress = `ta${string}`;

/**
 * A crypto credential
 * @public
 */ export type CryptoCredential = `${string}.${string}.mastercard`;

/**
 * The destination of a transaction either a blockchain address, a CAIP-19 address, or a travel address.
 * @public
 */
export type Destination =
  | BlockchainAddress
  | CAIP10
  | CryptoCredential
  | TravelAddress;

/**
 * The source of a transaction
 * @public
 */
export type Source = BlockchainAddress | CAIP10;

/**
 * A Uniform Resource Identifier
 * @public
 */
export type URI = string;

/**
 * A Decentralized Identifier
 * @public
 */
export type DID = `did:${string}:${string}`;

/**
 * A LEI Legal Entity Identifier
 * @public
 */
export type LEI = string;

/**
 * 3 letter ISO currency code
 * @public
 */
export type ISOCurrency = string;

/**
 * The theme of the Notabene SDK
 * @public
 */
export type Theme = {
  mode: 'light' | 'dark'; // Defaults to 'light'
  primaryColor?: string;
  secondaryColor?: string;
  fontFamily?: string;
  logo?: string;
};

/**
 * The type of Agent. Either a wallet or a VASP
 * @public
 */
export enum AgentType {
  PRIVATE = 'WALLET',
  VASP = 'VASP',
}

/**
 * Who is the agent acting on behalf of the counterparty
 * @public
 */
export interface Agent {
  did: DID;
  type: AgentType;
  logo?: URI;
  url?: URI;
  name?: string;
  verified?: boolean;
}

/**
 * The type of counterparty. Either a natural person or a legal person. If the customer is the same as the counterparty then the counterparty is a self.
 * @public
 */
/**
 * Enum defining the types of persons/entities in a transaction
 *
 * @remarks
 * This classification system aligns with FATF travel rule requirements and defines:
 * - NATURAL: Individual human persons acting in their own capacity
 * - LEGAL: Registered organizations, companies, or other legal entities
 * - SELF: When the counterparty is the same as the customer (first party transaction)
 *
 * The type affects what information must be collected and transmitted as part of
 * travel rule compliance. Different verification and due diligence requirements
 * apply to each type.
 *
 * @see {@link NaturalPerson} For natural person data requirements
 * @see {@link LegalPerson} For legal person data requirements
 * @public
 */
export enum PersonType {
  NATURAL = 'natural',
  LEGAL = 'legal',
  SELF = 'self', // first party
}

/**
 * A VASP agent acting on behalf of the counterparty
 * @public
 */
export interface VASP extends Agent {
  lei?: LEI;
  logo?: URI;
  website?: URI;
  countryOfRegistration?: ISOCountryCode;
}

/**
 * A wallet agent acting on behalf of the counterparty
 * @public
 */
export interface Wallet extends Agent {
  proof: OwnershipProof;
  wallet_connect_id?: string;
}

/**
 * The counterparty of a transaction.
 * @public
 */
/**
  * Interface representing a party involved in a transaction other than the initiator
  *
  * @remarks
fines the core properties that identify and describe a counterparty:
  * - name: The display or legal name of the counterparty
  * - accountNumber: An account identifier/reference number
  * - did: Decentralized identifier for the counterparty
  * - type: Classification as natural person, legal entity, or self
  * - verified: Whether the counterparty's identity has been verified
  * - geographicAddress: Physical/mailing address information
  * - nationalIdentification: Government-issued ID details
  * - website: Official web presence
  * - phone: Contact phone number
  * - email: Contact email address
  *
  * This interface serves as the base for more specific counterparty types:
  * @see {@link NaturalPerson} For individual person properties
  * @see {@link LegalPerson} For organization/entity properties
  *
  * @public
  */
export interface Counterparty {
  name?: string;
  accountNumber?: string;
  did?: DID;
  type?: PersonType;
  verified?: boolean;
  geographicAddress?: Address;
  nationalIdentification?: NationalIdentification;
  website?: URI;
  phone?: string;
  email?: string;
}

/**
 * Interface representing a natural person (individual) involved in a transaction
 *
 * @remarks
 * Extends the baseinterface to add properties specific to individual persons:
 * - type: Must be PersonType.NATURAL to identify as an individual
 * - dateOfBirth: Optional ISO format birth date for identity verification
 * - placeOfBirth: Optional birth place for identity verification
 * - countryOfResidence: Optional ISO country code of current residence
 * - name: Required full legal name of the individual
 *
 * This interface captures the additional identifying information required for
 * natural persons under FATF Travel Rule requirements. The properties align
 * with standard KYC (Know Your Customer) data collection practices.
 *
 * @see {@link Counterparty} For base properties common to all counterparties
 * @see {@link PersonType} For person type classification
 * @public
 */
export interface NaturalPerson extends Counterparty {
  type: PersonType.NATURAL;
  dateOfBirth?: ISODate;
  placeOfBirth?: string;
  countryOfResidence?: ISOCountryCode;
  name: string;
}
/**
 * Field names for NaturalPerson
 * @public
 */
export type NaturalPersonFieldName =
  | 'name' // Full legal name
  | 'website' // Primary website of entity
  | 'email' // Contact email
  | 'phone' // Contact mobile phone
  | 'geographicAddress' // Address string
  | 'nationalIdentification' // National Identification number
  | 'dateOfBirth' // Date of Birty YYYY-MM-DD
  | 'placeOfBirth' // Place of Birth
  | 'countryOfResidence'; // ISO Country code of residence of Natural Person

/**
 * Field properties by field name for Natural persons
 * @public
 */
export type NaturalPersonFields = {
  [name in NaturalPersonFieldName]?: FieldOptions;
};

/**
 * Interface representing a legal entity (organization/company) involved in a transaction
 *
 * @remarks
 * Extends the baseface to add properties specific to legal entities:
 * - type: MustPersonType.LEGAL to identify as an organization
 * - name: Required registered legal name of the entity
 * - lei: Optional Legal Entity Identifier for regulated entities
 * - logo: Optional URI to the organization's logo image
 * - countryOfRegistration: Optional ISO country code where entity is registered
 *
 * This interface captures the additional identifying information required for
 * legal persons under FATF Travel Rule requirements. The properties align with
 * standard business KYC (Know Your Businessta collection practices.
 *
 * @see {@link Counterparty} For base properties common to all counterparties
 * @see {@link PersonType} For person type classification
 * @see {@link LEI} For Legal Entity Identifier format
 * @public
 */
export interface LegalPerson extends Counterparty {
  type: PersonType.LEGAL;
  name: string;
  lei?: LEI;
  logo?: URI;
  countryOfRegistration?: ISOCountryCode;
}

export type LegalPersonFieldName =
  | 'name' // Full legal name
  | 'lei' // Legal Entity Identifier
  | 'website' // Primary website of entity
  | 'email' // Contact email
  | 'phone' // Contact mobile phone
  | 'geographicAddress' // Address string
  | 'nationalIdentification' // National Identification number
  | 'countryOfRegistration'; // ISO Country code of registration of Legal Person

/**
 * Field properties by field name
 * @public
 */
export type LegalPersonFields = {
  [name in LegalPersonFieldName]?: FieldOptions;
};

/**
 * Fields specific to the originator of a transaction
 * @public
 */
type OriginatorFields = {
  source?: Source;
};

/**
 * Fields specific to the beneficiary of a transaction
 * @public
 */
type BeneficiaryFields = {
  destination?: Destination;
};

/**
 * Fields specific to a deposit request
 * @public
 */
type DepositRequestFields = {
  destination: BlockchainAddress | CAIP10;
  asset: TransactionAsset;
  amountDecimal?: number;
  travelAddress?: TravelAddress;
  cryptoCredential?: CryptoCredential;
};

type RequestID = UUID;

/**
 * Base interface for requests sent to SDK components
 *
 * @remarks
 * Defines core properties that all component requests share:
 * - Optional unique request ID for tracking/correlating requests and responses
 * - Optional customer detailsfor pre-filling component data
 *
 * This interface is extended by specific request types like:
 * - Transaction requests for sending/receiving assets
 * - Connection requests for establishing VASP to VASP communication
 *
 * @see {@link Transaction} For transaction-specific request properties
 * @see {@link ConnectionRequest} For connection-specific request properties
 * @public
 */
export interface ComponentRequest {
  requestId?: RequestID;
  customer?: Counterparty;
}

/**
 * Core transaction interface representing a crypto asset transfer between parties
 *
 * @remarks
 * Extends ComponentRequest to add transaction-specific properties:
 * - agent: The entity facilitating/executing the transaction
 * - counterparty: The other party involved in the transaction
 * - asset: The cryptocurrency or token being transferred
 * - amountDecimal: The amount to transfer in decimal format
 * - proof: Optional ownership proof verifying control of involved addresses
 * - assetPrice: Optional price information in a fiat currency
 *
 * This interface serves as the base for specific transaction types like:
 * - Withdrawals for sending assets out
 * - Deposits for receiving assets
 * - Deposit requests for requesting asset transfers
 *
 * @see {@link Withdrawal} For withdrawal-specific transaction properties
 * @see {@link Deposit} For deposit-specific transaction properties
 * @see {@link Agent} For agent details
 * @see {@link Counterparty} For counterparty information
 * @public
 */
export interface Transaction extends ComponentRequest {
  agent: Agent;
  counterparty: Counterparty;
  asset: TransactionAsset;
  amountDecimal: number;
  proof?: OwnershipProof;
  assetPrice?: {
    price: number;
    currency: ISOCurrency;
  };
}

export interface RefreshSource {
  url: URI;
  key: string;
}

export interface Refreshable {
  refreshSource?: RefreshSource;
}

/**
 * Extended transaction interface for deposits
 * @public
 */
export interface DepositTransaction extends Transaction {
  transactionId?: string; // Optional transactionId of a Notabene transaction. Will be returned with the payload to assist updating the Transaction
}

/**
 * An object representing a withdrawal transaction
 * @public
 */
export interface Withdrawal
  extends BeneficiaryFields,
    Transaction,
    Refreshable {}

/**
 * An object representing a deposit transaction
 * @public
 */
export interface Deposit
  extends OriginatorFields,
    DepositTransaction,
    Refreshable {}

/**
 * An object representing a request for a deposit
 * @public
 */
export interface DepositRequest
  extends DepositRequestFields,
    ComponentRequest {}

/**
 * An object representing options for a Deposit Request
 * @public
 */
export interface DepositRequestOptions {
  showQrCode?: boolean; // Defaults to true
}

/**
 * An object representing a connection request
 * @public
 */
export interface ConnectionRequest extends ComponentRequest {
  asset: TransactionAsset;
}

/**
 * An object representing options for a Connection Request
 * @public
 */

export type ConnectionOptions = Omit<
  TransactionOptions,
  'allowedAgentTypes' | 'allowedCounterpartyTypes' | 'vasps' | 'fields' | 'hide'
>;

/**
 * The verification status of a transaction
 * @public
 */
export enum Status {
  EMPTY = 'empty',
  VERIFY = 'verify',
  PENDING = 'pending',
  VERIFIED = 'verified',
  BANNED = 'banned',
}

/**
 * Represents a legacy V1 API asset format supporting both Notabene and CAIP-19 identifiers
 *
 * @remarks
 * Used for backwards compatibility with V1 API transaction payloads:
 * - Can be either a simple Notabene asset string
 * - Or an object containing a CAIP-19 identifier
 *
 * @example "ETH_USDT" // Notabene asset format
 * @example \{ caip19: "eip155:1/erc20:0x6b175474e89094c44da98b954eedeac495271d0f" \} // CAIP-19 format
 * @see {@link NotabeneAsset} For Notabene asset format
 * @see {@link CAIP19} For CAIP-19 asset format
 * @public
 */
export type V1Asset = NotabeneAsset | { caip19: CAIP19 };

/**
 * Transaction payload suitable for calling Notabene v1 tx/create
 * @public
 */
export type V1Transaction = {
  transactionAsset: V1Asset;
  transactionAmount: string;
  originatorEqualsBeneficiary?: boolean;
  originatorVASPdid: DID;
  beneficiaryVASPdid: DID;
  beneficiaryProof?: OwnershipProof;
  originatorProof?: OwnershipProof;
  originator?: Originator;
  beneficiary: Beneficiary;
  transactionId?: string;
};

/**
 * Base response interface for all SDK component operations
 *
 * @remarks
 * Provides standardized response propertiesfor component interactions:
 * - requestID: Links response back to the originating request
 * - valid: Boolean indicating if the operation was valid/successful
 * - status: Current verification status of the operation
 * - errors: Array of validation errors if any occurred
 *
 * This interface is extended by specific response types like:
 * - TransResponse for transaction operations
 * - ConnectionResponse for VASP connection operations
 *
 * @see {@link Status} For possible status values
 * @see {@link ValidationError} For error structure
 * @see {@link TransactionResponse} For transaction-specific responses
 * @public
 */
export interface ComponentResponse {
  requestID: RequestID;
  valid: boolean;
  status: Status;
  errors: ValidationError[];
}

/**
 * Response interface for transaction-related operations
 *
 * @remarks
 * Extends ComponentResponse to add transaction-specific response data:
 * - value: The resulting transaction value of generic type V
 * - ivms101: IVMS 101 travel rule data for the transaction
 * - proof: Optional ownership proof details if required
 * - txCreate: Optional V1 transaction payload for legacy API compatibility
 * - txUpdate: Optional V1 transaction payload for legacy API compatibility
 *
 * @typeParam V - Type of the transaction value being returned
 *
 * @see {@link ComponentResponse} For base response properties
 * @see {@link IVMS101} For travel rule data structure
 * @see {@link OwnershipProof} For proof details
 * @see {@link V1Transaction} For legacy transaction format
 * @public
 */
export interface TransactionResponse<V> extends ComponentResponse {
  value: V;
  ivms101: IVMS101;
  proof?: OwnershipProof;
  txCreate?: V1Transaction;
  txUpdate?: V1Transaction;
}

/**
 * Validation error
 * @public
 */
export type ValidationError = {
  attribute: string;
  message: string;
};

/**
 * Field properties
 * @public
 */
export type FieldOptions =
  | boolean
  | {
      optional: boolean; // Shown but optional
      transmit: boolean; // Transmit as part of IVMS 101 to counterparty
    };

/**
 * Field type configuration
 * @public
 */

export type FieldTypes = {
  naturalPerson?: NaturalPersonFields;
  legalPerson?: LegalPersonFields;
};

/**
 * VASP Visibility Control representing allow/deny behaviour for VASP visibility
 *
 * @remarks
 * - ALLOWED: Include VASPs you have explicitly allowed in the Notabene Network
 * - PENDING: Include VASPs neither allowed nor denied
 * @public
 */
export enum VASPSearchControl {
  ALLOWED = 'allowed',
  PENDING = 'pending',
}

/**
 * Options for which VASPs to be searchable
 * @public
 */
export type VASPOptions = {
  addUnknown?: boolean;
  onlyActive?: boolean;
  searchable?: VASPSearchControl[]; // If array left empty all VASPs will be searchable
};

/**
 * Sections in a WithdrawalAssist screen
 *
 * @alpha
 */
export enum ValidationSections {
  ASSET = 'asset',
  DESTINATION = 'destination',
  COUNTERPARTY = 'counterparty',
  AGENT = 'agent',
}
/**
 * Specify what to do under the provided threshold.
 *
 * Eg. to allow self-declaration for all transactions under 1000 EUR
 *
 * Note to support threshold you MUST include the Asset Price in the Transaction
 *
 * @see {@link Transaction} Transaction object
 *
 * @public
 */

export interface ThresholdOptions {
  threshold: number; // The threshold amount eg 1000
  currency: ISOCurrency; // Currency of threshold
  proofTypes?: ProofTypes[]; // If left empty no proof will be required under threshold
}
/**
 * Configuration options for Transaction components
 * @public
 */
export interface TransactionOptions {
  proofs?: {
    reuseProof?: boolean; // Defaults true
    microTransfer?: {
      destination: BlockchainAddress;
      amountSubunits: string;
      timeout?: number; // Time to verify in seconds
    };
    fallbacks?: ProofTypes[];
    deminimis?: ThresholdOptions;
  };
  allowedAgentTypes?: AgentType[]; // Defaults to All
  allowedCounterpartyTypes?: PersonType[]; // Defaults to All
  fields?: FieldTypes;
  vasps?: VASPOptions;
  hide?: ValidationSections[]; // You can hide a specific section of the component by listing it here
}
/**
 * Component Message Type enum representing different message types that can be sent
 * between the host and component.
 *
 * @remarks
 * - COMPLETE: Indicates a completed operation with response data
 * - RESIZE: Request to adjust component size/dimensions
 * - RESULT: Operation result notification
 * - READY: Component is initialized and ready
 * - INVALID: Validation failed with errors
 * - ERROR: Operation encountered an error
 * - CANCEL: Operation was cancelled
 * @public
 */
export enum CMType {
  COMPLETE = 'complete',
  RESIZE = 'resize',
  RESULT = 'result',
  READY = 'ready',
  INVALID = 'invalid',
  ERROR = 'error',
  CANCEL = 'cancel',
}

/**
 * Represents a completed component message
 * @typeParam T - The overall Value type being returned
 * @param response - The Response object which wraps T
 * @public
 */
export type Completed<T> = {
  type: CMType.COMPLETE;
  response: TransactionResponse<T>;
};

/**
 * Represents a ready component message
 * @public
 */
export type Ready = {
  type: CMType.READY;
};

/**
 * Represents a resize request component message. This is handled by the library.
 * @internal
 */
export type ResizeRequest = {
  type: CMType.RESIZE;
  height: number;
};

/**
 * Represents an error identifier code
 * @public
 */
export enum ErrorIdentifierCode {
  SERVICE_UNAVAILABLE = 'SERVICE_UNAVAILABLE',
  WALLET_CONNECTION_FAILED = 'WALLET_CONNECTION_FAILED',
  WALLET_NOT_SUPPORTED = 'WALLET_NOT_SUPPORTED',
  TOKEN_INVALID = 'TOKEN_INVALID',
}

/**
 * Represents an error component message
 * @param message - Error message
 * @param description - Description of the error message
 * @param identifier - Identifier code of the error message
 * @public
 */
export type Error = {
  type: CMType.ERROR;
  message: string;
  description?: string;
  identifier?: ErrorIdentifierCode;
};

/**
 * Represents a cancel component message
 * @internal
 */
export type Cancel = {
  type: CMType.CANCEL;
};

/**
 * Represents an invalid value component message
 * @typeParam T - The overall Value type being returned
 * @param value - The current Partial value
 * @param errors - Array of validation errors
 * @internal
 */
export type InvalidValue<T> = {
  type: CMType.INVALID;
  value: Partial<T>;
  errors: ValidationError[];
};

/**
 * Union type representing all possible messages that can be sent from a component
 *
 * @remarks
 * Components communicate their state and results back to the host application
 * through these message types:
 * - Completed: Operation finished successfully with response data
 * - Cancel: User cancelled the operation
 * - Error: Operation failed with error message
 * - ResizeRequest: Component needs to adjust its dimensions
 * - InvalidValue: Validation failed with current partial value
 *
 * @typeParam T - The value type that will be returned in Completed messages
 *
 * @see {@link Completed} For successful completion message format
 * @see {@link Cancel} For cancellation message format
 * @see {@link Error} For error message format
 * @see {@link ResizeRequest} For resize message format
 * @see {@link InvalidValue} For validation failure message format
 * @public
 */
export type ComponentMessage<T> =
  | Completed<T>
  | Cancel
  | Error
  | Ready
  | ResizeRequest
  | InvalidValue<T>;

/**
 * Host Message Type enum representing different message types that can be sent
 * from the host application.
 *
 * @remarks
 * - UPDATE: Message to update component value/state
 * - REQUEST_RESPONSE: Message requesting a response from component
 * @public
 */
export enum HMType {
  UPDATE = 'update',
  REQUEST_RESPONSE = 'requestResponse',
}

/**
 * Message type for updating component state and configuration from host application
 *
 * @remarks
 * Defines the structure of update messages sent from host to component:
 * - type: Identifies this as an update message
 * - value: New partial state/data to update the component with
 * - options: Optional configuration parameters to modify component behavior
 *
 * The host can use this to dynamically update both the component's data
 * and its configuration without requiring a full reload/reinitialize.
 *
 * @typeParam T - The type of the value being updated
 * @typeParam O - The type of the optional configuration parameters
 *
 * @see {@link HMType} For message type constants
 * @see {@link HostMessage} For full host message type union
 * @public
 */
export type UpdateValue<T, O> = {
  type: HMType.UPDATE;
  value: Partial<T>;
  options?: O;
};

/**
 * Union type representing all possible messages that can be sent from the host application
 * to a component
 *
 * @remarks
 * Currently only supports update messages which allow the host to modify component
 * and configuration. The host uses these messages to communicate changes to the component
 * without requiring full reinitialization.
 *
 * @typeParam T - The value type that components operate on
 * @typeParam O - The options type used to configure component behavior
 *
 * @see {@link UpdateValue} For the structure of update messages
 * @see {@link HMType} For message type constants
 * @public
 */
export type HostMessage<T, O> = UpdateValue<T, O>;

/**
 * Options for callback and redirect URIs
 * @public
 */
export interface CallbackOptions {
  callback?: URI;
  redirectUri?: URI;
}

/**
 * Status of the ownership proof verification process
 *
 * @remarks
 * Represents the different states that an ownership proof can be in during and after verification:
 * - PENDING: Initial state where verification is in progress or awaiting processing
 * - FAILED: The proof was rejected due to failing verification checks
 * - FLAGGED: The proof requires manual review due to suspicious or unclear verification results
 * - VERIFIED: The proof has passed all verification checks successfully
 *
 * @public
 */
export enum ProofStatus {
  PENDING = 'pending', // Verification is pending
  FAILED = 'rejected', // Rejected
  FLAGGED = 'flagged', // Flagged for manual review
  VERIFIED = 'verified', // Verified
}

/**
 * Types of ownership proofs supported by the system
 *
 * @remarks
 * Supported proof types:
 * - SelfDeclaration: User self-declares ownership without cryptographic proof
 * - EIP191: Ethereum personal signature following EIP-191 standard
 * - SIWE: Sign-In with Ethereum message signature (EIP-4361)
 * - EIP712: Ethereum typed data signature following EIP-712 standard
 * - BIP137: Bitcoin message signature following BIP-137
 * - BIP322: Bitcoin message signature following BIP-322
 * - TIP191: Tron message signing
 * - ED25519: Ed25519 signature (used in Solana)
 * - XPUB: Extended public key signature for HD wallets
 * - MicroTransfer: Proof via small blockchain transaction
 * - Screenshot: Image proof of ownership/access
 * - CIP8: Cardano message signing standard (CIP-8)
 *
 * @see {@link SignatureProof} For signature-based proofs
 * @see {@link DeclarationProof} For self-declaration proofs
 * @see {@link MicroTransferProof} For transaction-based proofs
 * @see {@link ScreenshotProof} For screenshot proofs
 * @public
 */
export enum ProofTypes {
  SelfDeclaration = 'self-declaration',
  SIWE = 'siwe',
  SIWX = 'siwx',
  EIP191 = 'eip-191',
  EIP712 = 'eip-712',
  EIP1271 = 'eip-1271',
  BIP137 = 'bip-137',
  BIP322 = 'bip-322',
  BIP137_XPUB = 'xpub',
  TIP191 = 'tip-191',
  ED25519 = 'ed25519',
  CIP8 = 'cip-8',
  MicroTransfer = 'microtransfer',
  Screenshot = 'screenshot',
  Connect = 'connect',
}

/**
 * Base interface for proving ownership of an account or address
 *
 * @remarks
 * TheOwnershipProof interface provides a common structure for different types of ownership verification:
 * - All proofs must specify their type from the supported ProofTypes enum
 * - Current verification status is tracked via ProofStatus
 * - Links the proof to a decentralized identifier (DID)
 * - Specifies the blockchain account/address being proven using CAIP-10 format
 *
 * This interface is extended by specific proof types like:
 * - SignatureProof for cryptographic signatures
 * - DeclarationProof for self-declarations
 * - MicroTransferProof for transaction-based proof
 * - ScreenshotProof for image-based verification
 *
 * @see {@link ProofTypes} For supported proof methods
 * @see {@link ProofStatus} For possible verification states
 * @see {@link CAIP10} For address format specification
 * @public
 */
export interface OwnershipProof {
  type: ProofTypes;
  status: ProofStatus;
  did: DID;
  address: CAIP10;
}

/**
 * Interface for signature-based ownership proofs that use cryptographic message signing
 *
 * @remarks
 * Extends the base OwnershipProface to add signature-specific properties:
 * - Supports multiple signature standards like EIP-191, EIP-712, BIP-137, SIWE
 * - Includes the cryptographic proof signature string
 * - Contains an attestation message that was signed
 * - Records which wallet provider was used for signing
 *
 * The signature proves ownership by demonstrating control of the private keys
 * associated with the claimed address.
 *
 * @see {@link ProofTypes} For supported signature types
 * @see {@link OwnershipProof} For base proof properties
 * @public
 */
export interface SignatureProof extends OwnershipProof {
  type:
    | ProofTypes.EIP191
    | ProofTypes.EIP712
    | ProofTypes.EIP1271
    | ProofTypes.BIP137
    | ProofTypes.BIP322
    | ProofTypes.BIP137_XPUB
    | ProofTypes.ED25519
    | ProofTypes.TIP191
    | ProofTypes.SIWX
    | ProofTypes.SIWE
    | ProofTypes.CIP8;

  proof: string;
  attestation: string;
  wallet_provider: string;
  xpub?: string;
  chainSpecificData?: {
    cardanoCoseKey?: string; // Cardano COSE key https://cips.cardano.org/cip/CIP-0030
  };
}

/**
 * Ownership Proof using Self Declaration
 * @public
 */
export interface DeclarationProof extends OwnershipProof {
  type: ProofTypes.SelfDeclaration;
  attestation: string;
  confirmed: boolean;
}

/**
 * Interface for recording that user connected their wallet.
 *
 * @remarks
 * - Records which wallet provider was used to connect
 *
 * The signature proves ownership by demonstrating control of the private keys
 * associated with the claimed address.
 *
 * @see {@link ProofTypes} For supported signature types
 * @see {@link OwnershipProof} For base proof properties
 * @public
 */
export interface ConnectionRecord extends OwnershipProof {
  type: ProofTypes.Connect;
  proof: string;
  attestation: string;
  wallet_provider: string;
}

/**
 * Ownership Proof using Micro Transfer
 * @public
 */
export interface MicroTransferProof extends OwnershipProof {
  type: ProofTypes.MicroTransfer;
  proof: string;
  chain: CAIP2;
  asset: CAIP19;
  destination: BlockchainAddress;
  amountSubunits: string;
}

/**
 * Ownership Proof using Screenshot
 * @public
 */
export interface ScreenshotProof extends OwnershipProof {
  type: ProofTypes.Screenshot;
  url: string;
}
