import { resolve } from 'path';
import { defineConfig } from 'vite';

export default defineConfig({
  build: {
    lib: {
      entry: resolve(__dirname, 'src/notabene.ts'),
      name: 'Notabene',
      formats: ['es', 'cjs'],
      fileName: 'notabene',
    },
  },
});
