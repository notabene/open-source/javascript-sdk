import pluginJs from '@eslint/js';
import eslintPluginPrettierRecommended from 'eslint-plugin-prettier/recommended';
import globals from 'globals';
import tseslint from 'typescript-eslint';

export default [
  { files: ['**/*.{js,mjs,cjs,ts}'] },
  { languageOptions: { globals: globals.browser } },
  pluginJs.configs.recommended,
  ...tseslint.configs.recommended,
  eslintPluginPrettierRecommended,
  {
    ignores: [
      'dist/**/*',
      'eslint.config.js',
      'vite.config.js',
      'public/js/notabene.js',
      'ts-out/**/*',
      'docs/**/*',
    ],
  },
  {
    rules: { '@typescript-eslint/no-explicit-any': 'off' },
  },
];
