<div align="center">

<img src="https://assets-global.website-files.com/5e68f0772de982756aa8c1a4/5eee5fb470215e6ecdc34b94_Full_transparent_black_1280x413.svg" height=50>
<br>
# Notabene SafeConnect Components JavaScript SDK

[![pipeline status](https://gitlab.com/notabene/open-source/javascript-sdk/badges/master/pipeline.svg)](https://gitlab.com/notabene/open-source/javascript-sdk/-/commits/master)
[![npm version](https://img.shields.io/npm/v/@notabene/javascript-sdk.svg)](https://www.npmjs.com/package/@notabene/javascript-sdk)
[![npm downloads](https://img.shields.io/npm/dm/@notabene/javascript-sdk.svg)](https://www.npmjs.com/package/@notabene/javascript-sdk)
[![Bundle Size](https://img.shields.io/bundlephobia/minzip/@notabene/javascript-sdk)](https://bundlephobia.com/package/@notabene/javascript-sdk)
[![Types](https://img.shields.io/npm/types/@notabene/javascript-sdk)](https://www.npmjs.com/package/@notabene/javascript-sdk)
[![License](https://img.shields.io/npm/l/@notabene/javascript-sdk)](https://gitlab.com/notabene/open-source/javascript-sdk/-/blob/main/LICENSE.md)
[![Dependencies](https://img.shields.io/librariesio/release/npm/@notabene/javascript-sdk)](https://libraries.io/npm/@notabene%2Fjavascript-sdk)

This library is the JavaScript SDK for loading the Notabene UX components in the front-end.

[Additional Documentation](https://devx.notabene.id/docs/embedded=ux)

</div>

## Table of Contents

- [Notabene SafeConnect Components JavaScript SDK](#notabene-safeconnect-components-javascript-sdk)
  - [Table of Contents](#table-of-contents)
  - [Installation](#installation)
  - [Quick Start](#quick-start)
  - [Core Concepts](#core-concepts)
    - [Authentication](#authentication)
  - [General Component Usage](#general-component-usage)
    - [Embedded Component](#embedded-component)
      - [Dynamic updates](#dynamic-updates)
    - [Modal](#modal)
    - [Popup](#popup)
    - [Linked Component](#linked-component)
  - [Components](#components)
  - [Assisted Withdrawal](#assisted-withdrawal)
    - [Parameters](#parameters)
    - [Configuration Options](#configuration-options)
  - [Connect Wallet](#connect-wallet)
    - [Parameters](#parameters-1)
    - [Configuration Options](#configuration-options-1)
  - [Deposit Request](#deposit-request)
    - [Parameters](#parameters-2)
  - [Deposit Assist](#deposit-assist)
    - [Parameters](#parameters-3)
  - [Error handling](#error-handling)
      - [Error reference](#error-reference)
  - [Transaction parameters](#transaction-parameters)
    - [Asset specification](#asset-specification)
    - [Transaction amount](#transaction-amount)
    - [Destination](#destination)
    - [Origin](#origin)
    - [Asset Price](#asset-price)
  - [Configuration](#configuration)
    - [Transaction Options](#transaction-options)
    - [Common use cases](#common-use-cases)
      - [Only allow first party transactions](#only-allow-first-party-transactions)
      - [Only VASP to VASP transactions](#only-vasp-to-vasp-transactions)
      - [Only Self-hosted wallet transactions](#only-self-hosted-wallet-transactions)
    - [Configuring ownership proofs](#configuring-ownership-proofs)
      - [Supporting Micro Transactions (aka Satoshi tests)](#supporting-micro-transactions-aka-satoshi-tests)
      - [Fallback Proof Options](#fallback-proof-options)
    - [Counterparty Field Properties](#counterparty-field-properties)
      - [Full Example](#full-example)
      - [Field reference](#field-reference)
  - [Locales](#locales)
  - [Theming](#theming)
  - [License](#license)

## Installation

There are two options for loading the Notabene SDK:

```bash
<script id="notabene" async src="https://unpkg.com/@notabene/javascript-sdk@next/dist/notabene.js"></script>
```

Or installing the library:

Using Yarn:

```bash
yarn add @notabene/javascript-sdk
```

Using NPM:

```bash
npm install @notabene/javascript-sdk
```

If you installed the library into your project, you can import it into your project:

```js
import Notabene from '@notabene/javascript-sdk';
```

## Quick Start

```js
// 1. Create Notabene instance
const notabene = new Notabene({
  nodeUrl: 'https://api.notabene.id',
  authToken: 'YOUR_CUSTOMER_TOKEN',
});

// 2. Create and mount withdrawal component
const withdrawal = notabene.createWithdrawalAssist({
  asset: 'ETH',
  destination: '0x1234...',
  amountDecimal: 1.0,
});
withdrawal.mount('#nb-withdrawal');

// 3. Handle completion
const { valid, value, txCreate } = await withdrawal.completion();
if (valid) {
  // Submit to your backend
}
```

## Core Concepts

### Authentication

Use the [customer token endpoint](https://devx.notabene.id/docs/customertoken) with your access token to receive a token with a customer's scope.

> ⚠️ **IMPORTANT** ⚠️
>
> When requesting the `customer token` you **must pass a unique `customerRef` per customer** for ownership proof reusability, otherwise you might encounter unwanted behavior.

Create a new Notabene instance:

```js
const notabene = new Notabene({
  nodeUrl: 'https://api.notabene.id', // use `https://api.notabene.dev` for testing
  authToken: '{CUSTOMER_TOKEN}',
  locale: 'de', // default locale = `en`
});
```

Use the same `nodeUrl` that you use to interact with the Notabene API.

## General Component Usage

Each component can be used in various ways depending on your use case.

### Embedded Component

This will let you embed the component into your existing withdrawal flow.

Create an html element to contain the component:

```html
<div id="nb-withdrawal/>
```

Instantiate the withdrawal element and mount it using the id from above

```js
const withdrawal = notabene.createWithdrawalAssist(tx, options);
withdrawal.mount('#nb-withdrawal');
```

The simplest way to get the result is to use:

```js
try {
  const { valid, value, txCreate, ivms101, proof } =
    await withdrawal.completion();
  if (valid) {
    // Submit result to your backend
  }
} catch (e) {
  console.error(e);
}
```

#### Dynamic updates

To update the component as users enter transaction details:

```js
withdrawal.update({
  asset: 'ETH',
  destination: '0x8d12a197cb00d4747a1fe03395095ce2a5cc6819',
  amountDecimal: 1.12,
});
```

To be notified once the validation is completed so you can submit the withdrawal to your back end:

```js
withdrawal.on('complete', { valid, value, txCreate, ivms101, proof } => ...)
```

To be notified of any validation errors use:

```js
withdrawal.on('error',error => ...)
```

To be notified when the component is Ready:

```js
withdrawal.on('ready', ({ type }) => {
  console.log(type === 'ready');
});
```

Calling `on` returns a function that will allow you to cleanly unsubscribe.

```js
const unsubscribe = withdrawal.on('complete', { valid, value, txCreate, ivms101, proof } => ...)

// Clean up
unsubscribe()

```

### Modal

All components support being opened in a modal using `openModal()`, which returns a promise.

```js
const withdrawal = notabene.createWithdrawalAssist(tx, options);
try {
  const { valid, value, txCreate, ivms101, proof } =
    await withdrawal.openModal();
  if (valid) {
    // Submit result to your backend
  }
} catch (e) {
  console.error(e);
}
```

### Popup

All components support being opened in a popup window using `popup()`, which returns a promise.

Many embedded wallets refuse to work in an iframe. In this case it is better to use a popup window.

Unfortunately there are also some restrictions on popup windows:

- [Popup window Restrictions](https://developer.mozilla.org/en-US/docs/Web/API/Window/open#restrictions)

* [Cross-Origin-Opener-Policy](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cross-Origin-Opener-Policy)

```js
const withdrawal = notabene.createWithdrawalAssist(tx, options);
try {
  const { valid, value, txCreate, ivms101, proof } = await withdrawal.popup();
  if (valid) {
    // Submit result to your backend
  }
} catch (e) {
  console.error(e);
}
```

### Linked Component

In some cases, in particular institutional or mobile apps you may prefer to link your customers to the component through an email or redirect the user to it in a mobile app.

```js
const withdrawal = notabene.createWithdrawalAssist(tx, options, {
  callback: /// a serverside backend url
  redirectUri: // URI of website or mobile app to redirect user to after completion
});

// NodeJS redirect. Link also works in an email.
res.redirect(withdrawal.url);
```

Bear in mind that this is a full screen view for your users.

The two parameters that should be configured are:

- `callback` - a URL for your serverside. On completion this will receive an HTTP POST with the result as a json body and the `authToken` as an `Authorization: Bearer` header.
- `redirectUri` - the user will be redirected here on completion. The result parameters will be json encoded in the URL fragment. You can use a mobile app schema to intercept these in your mobile app.

**Note** for data privacy reasons the callback will be coming from your users web browser and not from our infrastructure, so no static IP is currently possible. Instead please check the `authToken` provided with the request.

## Components

## Assisted Withdrawal

The Withdrawal Assist component helps you collect additional required information from your user during a standard crypto withdrawal process.

```js
const withdrawal = notabene.createWithdrawalAssist({
  asset: 'ETH',
  destination: '0x...',
  amountDecimal: 1.23,
  assetPrice: {
    currency: 'USD', // ISO currency code
    price: 1700.12, // Asset price
  },
});
```

### Parameters

- `asset`: The cryptocurrency or token being transferred. See [Asset Specification](#asset-specification)
- `destination`: The destination or blockchain address for the withdrawal. See [Destination](#destination)
- `amountDecimal`: The amount to transfer in decimal format. See [Transaction Amount](#transaction-amount)
- `assetPrice`: Optional price information in a fiat currency. See [Asset Price](#asset-price)

If any of the required parameters are missing the component will just show the Notabene badge.

### Configuration Options

Include configuration Options as a second optional parameter:

```js
const withdrawal = notabene.createWithdrawalAssist(
  {
    asset: 'ETH',
    destination: '0x...',
    amountDecimal: 1.23,
    assetPrice: {
      currency: 'USD', // ISO currency code
      price: 1700.12, // Asset price
    },
  },
  {
    proofs: {
      microTransfer: {
        destination: '0x...',
        amountSubunits: '12344',
        timeout: 86440,
      },
    },
  },
);
```

See [Transaction Options](#transaction-options)

## Connect Wallet

The Connect Wallet component helps you collect and verify the address of your users self-hosted wallet in one go.

```js
const connect = notabene.createConnectWallet({
  asset: 'ETH',
});

const { proof, txCreate } = await connect.openModal();
```

### Parameters

- `asset`: The cryptocurrency or token being transferred. See [Asset Specification](#asset-specification)

### Configuration Options

Include configuration Options as a second optional parameter:

```js
const connect = notabene.createConnectWallet(
  {
    asset: 'ETH',
  },
  {
    proofs: {
      microTransfer: {
        destination: '0x...',
        amountSubunits: '12344',
        timeout: 86440,
      },
    },
  },
);
```

## Deposit Request

The Deposit Request lets your customers request deposits that are fully Travel Rule compliant.

```js
const withdrawal = notabene.createDepositRequest({
  asset: 'ETH',
  destination: '0x...',
  amountDecimal: 1.23,
  customer: {
    name: 'John Smith',
  },
});
```

### Parameters

- `asset`: The cryptocurrency or token being transferred. See [Asset Specification](#asset-specification)
- `destination`: The destination or blockchain address for the withdrawal. See [Destination](#destination)
- `amountDecimal`: Optional amount to deposit in decimal format. See [Transaction Amount](#transaction-amount)
- `customer`: Optional Customer object containing their name

If any of the required parameters are missing the component will just show the Notabene badge.

## Deposit Assist

The Deposit Assist component helps you collect missing Travel Rule data after a deposit has already been recorded on-chain. For example, if the deposit arrived with incomplete originator information, you can use Deposit Assist to request this information from your end-user.

```js
const deposit = notabene.createDepositAssist(
  {
    asset: 'ETH',
    amountDecimal: 1.23,
    origin: '0x...',
    destination: '0x...',
    transactionId: "UUID"
  },
  {
    // Optional transaction options
  },
);
```

### Parameters

- `asset`: The cryptocurrency or token being transferred. See [Asset Specification](#asset-specification)
- `origin`: The origin or blockchain address for the deposit. See [Origin](#origin)
- `destination`: The destination or blockchain address for the deposit. See [Destination](#destination)
- `amountDecimal`: Optional amount to deposit in decimal format. See [Transaction Amount](#transaction-amount)
- `transactionId`: Optional transactionId of a Notabene transaction. Will be returned with the payload to assist updating the Transaction

If any of the required parameters are missing the component will just show the Notabene badge.

---

## Error handling

If any error occurs, the `error` event is passed containing a message.

```ts
withdrawal.on('error', (error) => ...)
```

An example error object.

```ts
type Error = {
  type: CMType.Error;
  message: string;
  description: string;
  identifier: ErrorIdentifierCode;
};
```
Errors can be handled in this way:

```ts
component.on('error', (error) => {
  if (error.type === CMType.Error) {
    switch (error.identifier) {
      case ErrorIdentifierCode.SERVICE_UNAVAILABLE:
        // handle the error
        break;
      //...
    }
  }
});
```

#### Error reference

|                ErrorIdentifierCode          |                   Description/Message                                                          | In Use   |
|--------------------------|----------------------------------------------------------------------------------------------------------|---|
| SERVICE_UNAVAILABLE      | The Notabene service is currently unavailable                                                            | ✅  |
| TOKEN_INVALID            | Auth Token is Invalid                                                                                    | ✅  |
| WALLET_CONNECTION_FAILED | The connection to the wallet service failed, possibly due to network issues or unsupported wallet types. | ✅  |
| WALLET_NOT_SUPPORTED     | The wallet used does not support the required functionality or blockchain.                               |   |

## Transaction parameters

### Asset specification

The `asset` field the following types of assets specified:

- `notabene_asset` code passed as a`string`. See [Notabene Assets Service](https://devx.notabene.id/docs/coins-decimals#assets-service-api).
- [CAIP-19](https://github.com/ChainAgnostic/CAIPs/blob/main/CAIPs/caip-19.md_) is a chain agnostic format allows you to support the widest amount of assets and blockchains including NFTs.
- [DTI](https://www.iso.org/standard/80601.html) is the ISO Digital Token Identifier format. See [DTI registry](https://dtif.org/registry-search/) for supported tokens.

### Transaction amount

Use one of the following

- `amountDecimal` A number specifying the amount in decimal format. Eg. `amountDecimal=1.1` would mean 1.1 of for example BTC or ETH.

### Destination

Specify the beneficiary address as `destination` using one of the following formats:

- [CAIP-10](https://github.com/ChainAgnostic/CAIPs/blob/main/CAIPs/caip-10.md_) is a chain agnostic format allows you to specify the specific blockchain and address
- [EIP-3770](https://eips.ethereum.org/EIPS/eip-3770) EVM URI
- [BIP-21](https://en.bitcoin.it/wiki/BIP_0021) Bitcoin URI
- Native blockchain address

### Origin

Specify the originator address as `origin` using one of the following formats:

- [CAIP-10](https://github.com/ChainAgnostic/CAIPs/blob/main/CAIPs/caip-10.md_) is a chain agnostic format allows you to specify the specific blockchain and address
- [EIP-3770](https://eips.ethereum.org/EIPS/eip-3770) EVM URI
- [BIP-21](https://en.bitcoin.it/wiki/BIP_0021) Bitcoin URI
- Native blockchain address

### Asset Price

The price of the asset is used to determine certain rules based on thresholds. We recommond you pass in your price like this:

```ts
assetPrice: {
  currency: 'USD', // ISO currency code
  price: 1700.12, // Asset price
};
```

## Configuration

### Transaction Options

Some components can be configured using an optional [TransactionOptions](./docs/types/interfaces/TransactionOptions.md) object.

The following shows the full set of options in typescript:

```ts
import Notabene, {
  AgentType,
  PersonType,
  ProofTypes,
} from '@notabene/javascript-sdk';

const options: TransactionOptions = {
  proofs: {
    reuseProof: true, // Defaults true
    microTransfer: {
      destination: '0x...',
      amountSubunits: '12344',
      timeout: 86440,
    },
    fallbacks: [ProofTypes.Screenshot, ProofTypes.SelfDeclaration], // js ['screenshot','self-declaration']
    deminimis: {
      threshold: 1000,
      currency: 'EUR',
      proofTypes: [ProofTypes.SelfDeclaration],
    },
  },
  allowedAgentTypes: [AgentType.PRIVATE, AgentType.VASP], // js ['WALLET','VASP']
  allowedCounterpartyTypes: [
    PersonType.LEGAL, // JS: 'legal'
    PersonType.NATURAL, // JS: 'natural'
    PersonType.SELF, // JS: 'self'
  ],
  fields: {
    naturalPerson: {
      name: true, // Default true
      website: { optional: true },
      email: true,
      phone: true,
      geographicAddress: false,
      nationalIdentification: false,
      dateOfBirth: false,
      placeOfBirth: false,
      countryOfResidence: true,
    },
    legalPerson: {
      name: true, // Default true
      lei: true, // Default true
      website: { optional: true }, // Default true
      email: true,
      phone: true,
      geographicAddress: false,
      nationalIdentification: false,
      countryOfRegistration: true,
    },
    vasps: {
      addUnknown: true, // Allow users to add a missing VASP - Defaults to false
      onlyActive: true, // Only list active VASPs - Default false
      searchable: [
        VASPSearchControl.ALLOWED, // JS: 'allowed'
        VASPSearchControl.PENDING, // JS: 'pending'
      ], // Control searches for VASPs - Defaults to undefined
    },
    hide: [ValidationSections.ASSET, ValidationSections.DESTINATION], // Don't show specific sections of component
  },
};
const withdrawal = notabene.createWithdrawalAssist(tx, options);
```

The options can additionally be updated dynamically with the `update()` function.

```js
withdrawal.update(
  {
    asset: 'ETH',
    destination: '0x8d12a197cb00d4747a1fe03395095ce2a5cc6819',
    amountDecimal: 1.12,
  },
  {
    proofs: {
      microTransfer: {
        destination: '0x...',
        amountSubunits: '12344',
        timeout: 86440,
      },
    },
  },
);
```

### Common use cases

#### Only allow first party transactions

```ts
const firstParty: TransactionOptions = {
  allowedCounterpartyTypes: [
    PersonType.SELF, // JS: 'self'
  ],
};
```

#### Only VASP to VASP transactions

```ts
const vasp2vasp: TransactionOptions = {
  allowedAgentTypes: [AgentType.VASP], // js ['VASP']
};
```

#### Only Self-hosted wallet transactions

```ts
const options: TransactionOptions = {
  allowedAgentTypes: [AgentType.PRIVATE], // js ['WALLET']
};
```

### Configuring ownership proofs

By default components support message signing proofs.

#### Supporting Micro Transactions (aka Satoshi tests)

You can support Micro Transfers (aka Satoshi tests) by adding a deposit address for the test.

Your compliance team will have to determine how to handle and verify these transactions in the rules engine or individually.

```ts
const options: TransactionOptions = {
  proofs: {
    microTransfer: {
      destination: '0x...',
      amountSubunits: '1234',
      timeout: 86440, // Optional timeout in seconds, which is displayed to the user
    },
    fallbacks: [ProofTypes.Screenshot, ProofTypes.SelfDeclaration], // js ['screenshot','self_declaration']
  },
};
```

Notabene does not currently verify these tests automatically as you likely already have the infrastructure to do so.

You will receive a response back from the component containing a proof object. For MicroTransfers it will look like this:

```ts
type MicroTransferProof {
  type: ProofTypes.MicroTransfer;
  status: ProofStatus.PENDING;
  did: DID;
  address: CAIP10; // CAIP10 account to be verified
  txhash: string; // Transaction Hash to verify
  chain: CAIP2; // CAIP2 identifier of blockchain
  amountSubunits: string; // Amount in subunits eg (satoshi or wei) to be verified
}
```

#### Fallback Proof Options

You may accept a few options if none of the other are available. We do not recommend them, as they do not provide sufficient proof. However many VASPs do allow them for now:

```ts
const options: TransactionOptions = {
  proofs: {
    fallbacks: [ProofTypes.Screenshot, ProofTypes.SelfDeclaration], // js ['screenshot','self_declaration']
  },
};
```

The two options are:

- `screenshot` Where a user is requested to upload a screenshot of their wallet
- `self-declaration` Where a user self declares that they control the wallet address

### Counterparty Field Properties

The fields requested from a customer about a counterparty can be configured with the fields object. You can configure required and optional fields individually for both natural and legal persons.

We recommend working closely with your compliance team for this. Bearing in mind that different jurisdictions have different rules.

Each field can be configured like this:

- `true` required field
- `false` don't show
- `{ optional: true }` show but don't require

Eg:

```ts
{
  naturalPerson: {
    website: { optional: true },
    email: true,
    phone: false,
  }
}
```

The above will always ask the user for the following for natural persons:

- `name` since it is on by default (you can disable it explicitly by setting it to `false`)
- `website` is show but is optional
- `email` is required

#### Full Example

```ts
const options: TransactionOptions = {
  fields: {
    naturalPerson: {
      name: true, // Default true
      website: { optional: true },
      email: true,
      phone: true,
      geographicAddress: false,
      nationalIdentification: false,
      dateOfBirth: {
        transmit: true,
      },
      placeOfBirth: false,
      countryOfResidence: true,
    },
    legalPerson: {
      name: true, // Default true
      lei: true, // Default true
      website: { optional: true }, // Default true
      email: true,
      phone: true,
      geographicAddress: false,
      nationalIdentification: false,
      countryOfRegistration: true,
    },
  },
};
```

#### Field reference

| Field name               | Natural | Legal | IVMS101 | description                                   |
| ------------------------ | ------- | ----- | ------- | --------------------------------------------- |
| `name`                   | ✅      | ✅    | 🟩      | Full name                                     |
| `email`                  | 🟩      | 🟩    | --      | Email (for your internal purposes)            |
| `website`                | --      | ✅    | --      | Business Website (for your internal purposes) |
| `phone`                  | 🟩      | 🟩    | --      | Mobile Phone (for your internal purposes)     |
| `geographicAddress`      | 🟩      | 🟩    | 🟩      | Residencial or business address               |
| `nationalIdentification` | 🟩      | 🟩    | 🟩      | National Identification number                |
| `dateOfBirth`            | 🟩      | --    | 🟩      | Date of birth                                 |
| `placeOfBirth`           | 🟩      | --    | 🟩      | Place of birth                                |
| `countryOfResidence`     | 🟩      | --    | 🟩      | Country of Residence                          |
| `lei`                    | --      | ✅    | 🟩      | LEI (Legal Entity Identifier)                 |
| `countryOfRegistration`  | --      | 🟩    | 🟩      | Country of Registration                       |

## Locales

See [locales](src/locales.ts) for the list of supported locales.

## Theming

You can optionally theme the UI when creating an instance of `Notabene`

```js
const notabene = new Notabene({
  nodeUrl: 'https://api.notabene.id',
  authToken: 'YOUR_CUSTOMER_TOKEN',
  theme: {
    mode: 'light', // 'light', 'dark'
    primaryColor: '#0a852d',
    secondaryColor: '#f5f7fa',
    fontFamily: 'Montserrat' // Google Fonts Supported
  }
});
```

## [License](LICENSE.md)

MIT © Notabene Inc.
